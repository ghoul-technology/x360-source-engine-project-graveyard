﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XSourceVMTViewer
{
    [AttributeUsage(AttributeTargets.Field)]
    public class EndianAttribute : Attribute
    {
        public Endianness Endianness { get; private set; }

        public EndianAttribute(Endianness endianness)
        {
            this.Endianness = endianness;
        }
    }
}
