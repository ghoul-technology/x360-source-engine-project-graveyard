﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XSourceVMTViewer
{
    public enum Endianness
    {
        BigEndian,
        LittleEndian
    }
}
