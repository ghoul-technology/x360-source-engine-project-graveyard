#include "Aimbot.h"

using namespace Aimbot;

CAimbot::CAimbot()
{
	choked = -1;
	aim_at_point = false;
	was_firing = false;
	was_firing_test = false;
	has_entity = false;
	can_autowall = false;
	can_shoot = false;
	shot_this_tick = false;
	there_is_a_target = false;
	TargetID = 0;
}

void CAimbot::Init()
{
	IsAimStepping = false;
	IsLocked = false;
	TargetID = -1;
}

void CAimbot::Draw()
{
}

float curtime_fixed(CUserCmd* ucmd) 
{
	auto local_player = g_pInterfaces->EntityList->GetClientEntity(g_pInterfaces->EngineClient->GetLocalPlayer());
	static int g_tick = 0;
	static CUserCmd* g_pLastCmd = nullptr;

	if (!g_pLastCmd || g_pLastCmd->hasbeenpredicted) 
	{
		g_tick = local_player->GetTickBase();
	}
	else 
	{
		++g_tick;
	}

	g_pLastCmd = ucmd;
	float curtime = g_tick * g_pInterfaces->GlobalVars->interval_per_tick;

	return curtime;
}

void RandomSeed(UINT seed)
{
	typedef void(*RandomSeed_t)(UINT);
	static RandomSeed_t m_RandomSeed = (RandomSeed_t)0x8871D60C;
	m_RandomSeed(seed);
	return;
}

bool IsAbleToShoot(CBaseEntity* pLocal)
{
	C_BaseCombatWeapon* pWeapon = (C_BaseCombatWeapon*)g_pInterfaces->EntityList->GetClientEntityFromHandle(pLocal->GetActiveWeaponHandle());
	if (!pLocal)return false;
	if (!pWeapon)return false;
	float flServerTime = pLocal->GetTickBase() * g_pInterfaces->GlobalVars->interval_per_tick;;
	return (!(pWeapon->NextPrimaryAttack() > flServerTime));
}

bool CAimbot::CanOpenFire(CBaseEntity* local)
{
	C_BaseCombatWeapon* entwep = (C_BaseCombatWeapon*)g_pInterfaces->EntityList->GetClientEntityFromHandle(local->GetActiveWeaponHandle());
	float flServerTime = (float)local->GetTickBase() * g_pInterfaces->GlobalVars->interval_per_tick;
	float flNextPrimaryAttack = entwep->NextPrimaryAttack();

	DbgPrint("time:%.6f - next attack:%.6f", flServerTime, flNextPrimaryAttack);

	return !(flNextPrimaryAttack > flServerTime);
}

template<class T, class U>
inline T clamp(T in, U low, U high)
{
	if (in <= low)
		return low;
	else if (in >= high)
		return high;
	else
		return in;
}

float GetLerpTimeX()
{
	int ud_rate = g_pInterfaces->Cvar->FindVar("cl_updaterate")->GetFloat();

	ConVar *min_ud_rate = g_pInterfaces->Cvar->FindVar("sv_minupdaterate");
	ConVar *max_ud_rate = g_pInterfaces->Cvar->FindVar("sv_maxupdaterate");

	if (min_ud_rate && max_ud_rate)
		ud_rate = max_ud_rate->GetFloat();

	float ratio = g_pInterfaces->Cvar->FindVar("cl_interp_ratio")->GetFloat();

	if (ratio == 0)
		ratio = 1.0f;

	float lerp = g_pInterfaces->Cvar->FindVar("cl_interp")->GetFloat();

	ConVar *c_min_ratio = g_pInterfaces->Cvar->FindVar("sv_client_min_interp_ratio");
	ConVar *c_max_ratio = g_pInterfaces->Cvar->FindVar("sv_client_max_interp_ratio");

	if (c_min_ratio && c_max_ratio && c_min_ratio->GetFloat() != 1)
		ratio = clamp(ratio, c_min_ratio->GetFloat(), c_max_ratio->GetFloat());

	return max(lerp, (ratio / ud_rate));
}

void CAimbot::Move(CUserCmd *pCmd, bool &bSendPacket)
{
	/*
	CBaseEntity* pLocalEntity = g_pInterfaces->EntityList->GetClientEntity(g_pInterfaces->EngineClient->GetLocalPlayer());

	C_BaseCombatWeapon* pWeapon = (C_BaseCombatWeapon*)g_pInterfaces->EntityList->GetClientEntityFromHandle(pLocalEntity->GetActiveWeaponHandle());


	if (!g_pInterfaces->EngineClient->IsConnected() || !g_pInterfaces->EngineClient->IsInGame() || !pLocalEntity->ValidEntity())
		return;

	//c_fakelag->Fakelag(pCmd, bSendPacket);
	if (options::menu.AntiAimTab.AEnable.GetState())
	{
		static int ChokedPackets = 1;
		//	std::vector<dropdownboxitem> spike = options::menu.MiscTab.fl_spike.items;

		if (!pWeapon)
			return;

		if ((ChokedPackets < 1 && pLocalEntity->GetHealth() <= 0.f && !(pWeapon->IsKnife() || pWeapon->IsC4())))
		{
			bSendPacket = false;
		}

		else
		{
			if (pLocalEntity->IsAlive() && pLocalEntity->GetMoveType() != MOVETYPE_LADDER)
			{
				c_antiaim->DoAntiAim(pCmd, bSendPacket);
			}
			ChokedPackets = 1;
		}
	}

	DoAimbot(pCmd, bSendPacket);
	DoNoRecoil(pCmd);
	//auto_revolver(pCmd);


	LastAngle = pCmd->viewangles;
	*/
}

inline float FastSqrt(float x)
{
	unsigned int i = *(unsigned int*)&x;
	i += 127 << 23;
	i >>= 1;
	return *(float*)&i;
}

#define square( x ) ( x * x )

void ClampMovement(CUserCmd* pCommand, float fMaxSpeed)
{
	if (fMaxSpeed <= 0.f)
		return;
	float fSpeed = (float)(FastSqrt(square(pCommand->forwardmove) + square(pCommand->sidemove) + square(pCommand->upmove)));
	if (fSpeed <= 0.f)
		return;
	if (pCommand->buttons & IN_DUCK)
		fMaxSpeed *= 2.94117647f;
	if (fSpeed <= fMaxSpeed)
		return;
	float fRatio = fMaxSpeed / fSpeed;
	pCommand->forwardmove *= fRatio;
	pCommand->sidemove *= fRatio;
	pCommand->upmove *= fRatio;
}

