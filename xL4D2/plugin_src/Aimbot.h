#ifndef AIMBOT_H
#define AIMBOT_H

#pragma once

#include <vector>

#include "Interfaces.h"
#include "common.h"
#include "ConVar.h"
#include "Entity.h"


using namespace std;

namespace Aimbot
{
	struct AntiaimData_t
	{
		AntiaimData_t(const float& dist, const bool& inair, int player)
		{
			this->flDist = dist;
			this->bInAir = inair;
			this->iPlayer = player;
		}
		float flDist;
		bool bInAir;
		int	iPlayer;
	};

	class CAimbot
	{
	public:
		CAimbot();
		~CAimbot();

	public:
		void Init();
		void Draw();
		int HitScan(IClientEntity * pEntity, IClientEntity * pLocal);
		bool CanOpenFire(CBaseEntity * local);
		bool should_baim(CBaseEntity * pEntity);
		bool should_prefer_head(CBaseEntity * pEntity);
		bool enemy_is_slow_walking(CBaseEntity * entity);
		bool TargetMeetsRequirements(CBaseEntity * pEntity, CBaseEntity * local);
		std::vector<int> head_hitscan();
		std::vector<int> upperbody_hitscan();
		std::vector<int> lowerbody_hitscan();
		std::vector<int> arms_hitscan();
		std::vector<int> legs_hitscan();
		int get_target_hp(IClientEntity* pLocal);
		int automatic_hitscan(IClientEntity * entity);

		void delay_shot(CBaseEntity * ent, CUserCmd * pcmd);
		void mirror_console_debug(CBaseEntity * the_nignog);
		void DoAimbot(CUserCmd *pCmd, bool &bSendPacket);
		void DoNoRecoil(CUserCmd *pCmd);
		void aimAtPlayer(CUserCmd * pCmd, CBaseEntity * pLocal);
		void Move(CUserCmd *pCmd, bool &bSendPacket);

	public:
		int choked;
		bool aim_at_point;
		bool was_firing;
		bool was_firing_test;
		bool has_entity;
		bool can_autowall;
		bool can_shoot;
		bool shot_this_tick;
		bool there_is_a_target;
		Vector target_point;
		Vector origin_fl;
		CUserCmd* cmd;
		int TargetID;
		CBaseEntity * pTarget;

	private:
		std::vector<AntiaimData_t> Entities;

		bool AimAtPoint(CBaseEntity * pLocal, Vector point, CUserCmd * pCmd, bool & bSendPacket);

		float FovToPlayer(Vector ViewOffSet, Vector View, CBaseEntity* pEntity, int HitBox);
		//	void Base_AntiAim(CUserCmd * pCmd, IClientEntity * pLocal);
		bool IsAimStepping;
		Vector LastAimstepAngle;
		Vector LastAngle;

		bool IsLocked;

		int HitBox;
		Vector AimPoint;
	};
};



#endif //AIMBOT_H