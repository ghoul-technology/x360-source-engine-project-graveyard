#ifndef CRC32_H
#define CRC32_H

#pragma once

unsigned int CRC32(void *pData, size_t iLen);

#endif //CRC32_h