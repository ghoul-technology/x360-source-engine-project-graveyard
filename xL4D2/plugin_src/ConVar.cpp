#include "ConVar.h"

char* ConVar::GetName()
{
	return GetFunction<char *(__thiscall*)(ConVar*)>( this, 5 )(this);
}

void ConVar::SetValue(const char* value)
{
	return GetFunction<void(__thiscall*)(ConVar*, const char*)>( this, 14 )(this, value);
}

void ConVar::SetValue(float value)
{
	return GetFunction<void(__thiscall*)(ConVar*, float)>( this, 15 )(this, value);
}

void ConVar::SetValue(int value)
{
	return GetFunction<void(__thiscall*)(ConVar*, float)>( this, 16 )(this, value);
}

float ConVar::GetFloat(void) const 
{
	return pParent->fValue;
}

int ConVar::GetInt(void) const 
{
	return pParent->nValue;
}

bool ConVar::GetBool()
{
	return !!GetInt();
}

const char* ConVar::GetString(void) const 
{
	return pParent->pszDefaultValue;
}

char* ConVar::GetDefault()
{
	return pszDefaultValue;
}