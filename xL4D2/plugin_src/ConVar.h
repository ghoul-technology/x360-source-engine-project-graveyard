#ifndef CONVAR_H
#define CONVAR_H

#pragma once

#include "common.h"

class ConVar
{
public:
	void SetValue(const char *value);
	void SetValue(float value);
	void SetValue(int value);
	void SetValue(void* value);

	char* GetName();
	char* GetDefault();
	float						GetFloat(void) const;
	int						GetInt(void) const;
	bool GetBool();
	void*							GetColor(void) const;
	const char*						GetString(void) const;

	char pad_0x0000[0x4]; //0x0000
	ConVar* pNext; //0x0004 
	__int32 bRegistered; //0x0008 
	char* pszName; //0x000C 
	char* pszHelpString; //0x0010 
	__int32 nFlags; //0x0014 
	char pad_0x0018[0x4]; //0x0018
	ConVar* pParent; //0x001C 
	char* pszDefaultValue; //0x0020 
	char* strString; //0x0024 
	__int32 StringLength; //0x0028 
	float fValue; //0x002C 
//	CVValue_t m_Value;
	__int32 nValue; //0x0030 
	__int32 bHasMin; //0x0034 
	float fMinVal; //0x0038 
	__int32 bHasMax; //0x003C 
	float fMaxVal; //0x0040 
	void* fnChangeCallback; //0x0044 

};//Size=0x0048

#endif //CONVAR_H