#include "CrackBot.h"

XINPUT_STATE state;

void CalcAngle(Vector src, Vector dst, Vector &angles)
{
	Vector delta = src - dst;
	double hyp = delta.Length2D(); 
	angles.y = (atan(delta.y / delta.x) * 57.295779513082f);
	angles.x = (atan(delta.z / hyp) * 57.295779513082f);
	angles[2] = 0.00;

	if (delta.x >= 0.0)
		angles.y += 180.0f;
}

void NormaliseViewAngle(Vector &angle)
{
	
	while (angle.y <= -180) angle.y += 360;
	while (angle.y > 180) angle.y -= 360;
	while (angle.x <= -180) angle.x += 360;
	while (angle.x > 180) angle.x -= 360;


	if (angle.x > 89) angle.x = 89;
	if (angle.x < -89) angle.x = -89;
	if (angle.y < -180) angle.y = -179.999;
	if (angle.y > 180) angle.y = 179.999;

	angle.z = 0;
	
}

void Normalize(Vector &vIn, Vector &vOut)
{
	float flLen = vIn.Length();
	if (flLen == 0){
		vOut.Init(0, 0, 1);
		return;
	}
	flLen = 1 / flLen;
	vOut.Init(vIn.x * flLen, vIn.y * flLen, vIn.z * flLen);
}

void AngleVectors(const Vector &angles, Vector *forward)
{
	Assert(forward);

	float	sp, sy, cp, cy;

	sy = sin(DEG2RAD(angles[1]));
	cy = cos(DEG2RAD(angles[1]));

	sp = sin(DEG2RAD(angles[0]));
	cp = cos(DEG2RAD(angles[0]));

	forward->x = cp*cy;
	forward->y = cp*sy;
	forward->z = -sp;
}

inline void VectorSubtract(const Vector& a, const Vector& b, Vector& c)
{
	c.x = a.x - b.x;
	c.y = a.y - b.y;
	c.z = a.z - b.z;
}

CCrackBot::CCrackBot() 
{
	this->m_bIsLocked = false;
	this->m_bIsAimStepping = false;
	this->m_dwTarget = 0;
}

CCrackBot::~CCrackBot() {}

void CCrackBot::Execute( CUserCmd* cmd )
{
	return;
	if( !XInputGetState( 0, &state ) == ERROR_SUCCESS )
	{
		DbgPrint("XInputGetState failed!\n");
		return;
	}

	if(((BOOL)state.Gamepad.wButtons & XINPUT_GAMEPAD_LEFT_SHOULDER))
	{
		pLocal = nullptr;
		pLocal = g_pInterfaces->EntityList->GetClientEntity( g_pInterfaces->EngineClient->GetLocalPlayer() );

		if (!pLocal || pLocal->GetLifeState())
			return;

		int iLocalWeapon = (int)((ULONG)pLocal->GetActiveWeaponHandle() & 0xFFF);
		C_BaseCombatWeapon* pWeaponEnt = reinterpret_cast<C_BaseCombatWeapon*>(g_pInterfaces->EntityList->GetClientEntity(iLocalWeapon));
		if (!pWeaponEnt)
			return;

		int curAmmo = pWeaponEnt->GetAmmoInClip();
		if (!(curAmmo > 1))
			return;

		this->m_dwTarget = GetBestTarget();
		if (this->m_dwTarget == 1337)
			return;

		CBaseEntity* pEntity = g_pInterfaces->EntityList->GetClientEntity(this->m_dwTarget);

		auto nBone = EngineUtils::GetHeadHitboxID(pEntity->GetClientClass()->m_ClassID);

		Vector hitbox = EngineUtils::GetHitboxPosition(pEntity, nBone);
		if(this->AimAtPoint(pLocal, hitbox, cmd))
		{
			static bool bFired = false;
			if (!bFired)
				cmd->buttons |= IN_ATTACK;

			bFired = !bFired;
		}
	}

	this->m_vLastAngle = cmd->viewangles;
}

float CCrackBot::FovToPlayer(Vector ViewOffSet, Vector View, CBaseEntity* pEntity, int aHitBox)
{

	CONST FLOAT MaxDegrees = 180.0f;

	Vector Angles = View;

	Vector Origin = ViewOffSet;

	Vector Delta(0, 0, 0);

	Vector Forward(0, 0, 0);

	AngleVectors(Angles, &Forward);

	Vector AimPos = EngineUtils::GetHitboxPosition(pEntity, aHitBox);

	VectorSubtract(AimPos, Origin, Delta);

	Normalize(Delta, Delta);

	FLOAT DotProduct = Forward.Dot(Delta);

	return (acos(DotProduct) * (MaxDegrees / PI));
}

bool CCrackBot::AimAtPoint(CBaseEntity* pLocal, Vector point, CUserCmd *pCmd)
{
	bool ReturnValue = false;

	if (point.Length() == 0) return ReturnValue;

	Vector angles;
	Vector src = pLocal->GetEyes();

	CalcAngle(src, point, angles);

	NormaliseViewAngle(angles);

	if (angles[0] != angles[0] || angles[1] != angles[1])
	{
		return ReturnValue;
	}

	this->m_bIsLocked = true;

	Vector ViewOffset = pLocal->GetOrigin() + pLocal->GetViewOffset();

	if (!this->m_bIsAimStepping)
		this->m_vLastAimstepAngle = this->m_vLastAngle;

	float fovLeft = FovToPlayer(ViewOffset, this->m_vLastAimstepAngle, g_pInterfaces->EntityList->GetClientEntity(this->m_dwTarget), 0);

	Vector AddAngs = angles - this->m_vLastAimstepAngle;

	ReturnValue = true;

	pCmd->viewangles = angles;
}

DWORD CCrackBot::GetBestTarget()
{
	DWORD nBestTarget = 1337;
	float fBestDistance = FLT_MAX;

	for (DWORD nIndex = 1; nIndex < g_pInterfaces->EntityList->GetHighestEntityIndex(); nIndex++)
	{
		auto pEntity = g_pInterfaces->EntityList->GetClientEntity( nIndex );
		if (!pEntity || pEntity == pLocal || !pEntity->ValidEntity())
			continue;

		if (pEntity->GetTeam() == pLocal->GetTeam())
			continue;

		auto nBone = EngineUtils::GetHeadHitboxID( pEntity->GetClientClass()->m_ClassID );
		if (!nBone)
			continue;

		if (!EngineUtils::IsVisible( pLocal, pEntity, nBone ))
			continue;

		if (pEntity->GetClientClass()->m_ClassID == L4D2ClassID::Witch && pEntity->GetSequence() == 4)
			continue;

		float fCurrentDistance = MathUtils::WorldDistance( pEntity->GetOrigin(), pLocal->GetOrigin() );
		if (fCurrentDistance < fBestDistance)
		{
			fBestDistance = fCurrentDistance;
			nBestTarget = nIndex;
		}
	}

	return nBestTarget;
}

void CCrackBot::CompensateRecoil( CUserCmd* cmd )
{
	auto p = pLocal->GetPunch();
	cmd->viewangles.x -= p.x;
	cmd->viewangles.y -= p.y;
}

#pragma optimize( "", off )  
void CCrackBot::CompensateSpread( CUserCmd* cmd )
{
	/*
	static const char* szHorizSpread = "CTerrorGun::FireBullet HorizSpread";
	static const char* szVertSpread = "CTerrorGun::FireBullet VertSpread";

	using FnSharedRandomFloat = float( __cdecl* )(const char*, float, float, int);
	using FnUpdateMaxSpread = void( __cdecl* )(C_BaseCombatWeapon*);

	static auto uSharedRandomFloat = reinterpret_cast<std::uintptr_t>(GetModuleHandleA( "client.dll" )) + 0x1A8170;
	static auto SharedRandomFloat = reinterpret_cast<FnSharedRandomFloat>(uSharedRandomFloat);

	static auto UpdateMaxSpread = reinterpret_cast<FnUpdateMaxSpread>((reinterpret_cast<std::uintptr_t>(GetModuleHandleA( "client.dll" ))) + 0x2ECAF0);

	static std::uintptr_t uSeed = uSharedRandomFloat + 7;
	static std::uintptr_t** pSeed = (std::uintptr_t**)uSeed;

	std::uintptr_t uOldSeed = **pSeed;
	**pSeed = cmd->random_seed;

	auto pWeapon = reinterpret_cast<C_BaseCombatWeapon*>(g_Interfaces->EntityList->GetClientEntityFromHandle( pLocal->WeaponHandle() ));
	if (!pWeapon)
		return;

	if (!(pWeapon->GetCSWpnData()->iMaxClip1 > 1))
		return;

	float fOldMaxSpread = pWeapon->MaxSpread();
	UpdateMaxSpread( pWeapon );
	float fMaxSpread = pWeapon->MaxSpread();

	float fSpreadX = SharedRandomFloat( szHorizSpread, -fMaxSpread, fMaxSpread, 0 );
	float fSpreadY = SharedRandomFloat( szVertSpread, -fMaxSpread, fMaxSpread, 0 );

	*(float*)(reinterpret_cast<std::uintptr_t>(pWeapon) + 0xD0C) = fOldMaxSpread;

	**pSeed = uOldSeed;

	cmd->viewangles.x -= fSpreadX;
	cmd->viewangles.y -= fSpreadY;
	*/
}
#pragma optimize( "", on ) 