#ifndef CRACKBOT_H
#define CRACKBOT_H

#pragma once

#include <vector>

#include "Interfaces.h"

#include "ConVar.h"
#include "EngineUtils.h"

class CCrackBot
{
public:
	CCrackBot();
	~CCrackBot();

public:
	void Execute( CUserCmd* cmd );

private:
	CBaseEntity* pLocal;

private:
	DWORD GetBestTarget();
	void CompensateSpread( CUserCmd* cmd );
	void CompensateRecoil( CUserCmd* cmd );
	bool AimAtPoint(CBaseEntity* pLocal, Vector point, CUserCmd *pCmd);
	float FovToPlayer(Vector ViewOffset, Vector View, CBaseEntity* pEntity, int iHitbox);

private:
	bool m_bIsLocked;
	bool m_bIsAimStepping;
	Vector m_vLastAimstepAngle;
	Vector m_vLastAngle;
	DWORD m_dwTarget;
};
#endif // CRACKBOT_H