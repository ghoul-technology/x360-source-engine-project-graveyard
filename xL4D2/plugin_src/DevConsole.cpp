#include "DevConsole.h"

CDevConsole* g_pDevConsole;

CDevConsole::CDevConsole(void)
{

}

CDevConsole::~CDevConsole(void)
{

}

void CDevConsole::HookSpew(PDWORD destination)
{
	DWORD* spewFuncAddr = (DWORD*)0x82C11E08;

//	DbgPrint("Detouring spew func to debugprint");

	CXboxUtils::PatchInJump((PDWORD)0x82C11E08, (DWORD)printf, FALSE);
}