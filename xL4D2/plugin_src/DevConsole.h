#ifndef DEVCONSOLE_H
#define DEVCONSOLE_H

#pragma once

#include "XboxUtils.h"

class CDevConsole
{
public:
	CDevConsole(void);
	~CDevConsole(void);

	void HookSpew(PDWORD destination);
};

extern CDevConsole* g_pDevConsole;

#endif //DEVCONSOLE_H