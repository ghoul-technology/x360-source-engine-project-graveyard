#include "ESP.h"

using namespace VESP;




static bool ScreenTransform( const Vector& point, Vector& screen )
{
	const matrix3x4& w2sMatrix = g_pInterfaces->EngineClient->WorldToScreenMatrix();

	screen.x = w2sMatrix[0][0] * point.x + w2sMatrix[0][1] * point.y + w2sMatrix[0][2] * point.z + w2sMatrix[0][3];
	screen.y = w2sMatrix[1][0] * point.x + w2sMatrix[1][1] * point.y + w2sMatrix[1][2] * point.z + w2sMatrix[1][3];
	screen.z = 0.0f;

	float w = w2sMatrix[3][0] * point.x + w2sMatrix[3][1] * point.y + w2sMatrix[3][2] * point.z + w2sMatrix[3][3];

	if (w < 0.001f) {
		screen.x *= 100000;
		screen.y *= 100000;
		return true;
	}

	float invw = 1.0f / w;
	screen.x *= invw;
	screen.y *= invw;

	return false;
}

bool WorldToScreen( const Vector & origin, Vector & screen )
{
	if (!ScreenTransform(origin, screen)) 
	{
		int iScreenWidth, iScreenHeight;
		g_pInterfaces->EngineClient->GetScreenSize(iScreenWidth, iScreenHeight);
		screen.x = (iScreenWidth / 2.0f) + (screen.x * iScreenWidth) / 2;
		screen.y = (iScreenHeight / 2.0f) - (screen.y * iScreenHeight) / 2;

		return true;
	}
	return false;
}

CESP::CESP()
{
	this->pLocal = nullptr;
	this->m_ESPFont = 0;
	this->m_szWatermark = new wchar_t[128];
	this->m_bInit = false;

	this->m_rainbowR = 0;
	this->m_rainbowG = 0;
	this->m_rainbowB = 0;

	this->m_RainbowColor = Color(this->m_rainbowR, this->m_rainbowG, this->m_rainbowB, 255);
}

void CESP::UpdateRainbow()
{
	if(this->m_rainbowR < 255)
	{
		this->m_rainbowR++;
	}
	else if(this->m_rainbowG < 255)
	{
		this->m_rainbowG++;
	}
	else if(this->m_rainbowB < 255)
	{
		this->m_rainbowB++;
	}
	else
	{
		this->m_rainbowR = 0;
		this->m_rainbowG = 0;
		this->m_rainbowB = 0;
	}
	this->m_RainbowColor.SetColor(this->m_rainbowR, this->m_rainbowG, this->m_rainbowB);
}

void CESP::Draw()
{
	DrawWatermark();

	pLocal = g_pInterfaces->EntityList->GetClientEntity( g_pInterfaces->EngineClient->GetLocalPlayer() );

	if (!pLocal)
		return;
	
	for (int nIndex = 1; nIndex < g_pInterfaces->EntityList->GetHighestEntityIndex(); nIndex++)
	{
		CBaseEntity* pClient = g_pInterfaces->EntityList->GetClientEntity(nIndex);

		if (!pClient || pClient == pLocal || pClient->IsDormant())
			continue;

		L4D2ClassID classId = (L4D2ClassID)pClient->GetClientClass()->m_ClassID;

		if (pClient->GetGroup() && pClient->ValidEntity())
			DrawPlayer( nIndex );
		else if (classId == L4D2ClassID::CWeaponSpawn || classId == L4D2ClassID::CWeaponAmmoSpawn)
			DrawDrop( nIndex );
		else if (pClient->GetTeam() == 2)
		{
			auto pWeapon = reinterpret_cast<C_BaseCombatWeapon*>(pClient);
			if (pWeapon->GetOwnerHandle() == INVALID_HANDLE_VALUE)
				DrawDrop( nIndex );
		}
	}
	
}

bool CESP::Initialize()
{
	m_ESPFont = 0x0000008a;

	if (!(MultiByteToWideChar( CP_UTF8, 0, "xL4D2 by CRACKbomber", -1, m_szWatermark, 128 ) > 0))
	{
		DbgPrint("ESP - Failed to create watermark!");
		return false;
	}

	DbgPrint("ESP - Successfully initialized.");
	m_bInit = true;

	return true;
}

void CESP::DrawWatermark()
{
	static wchar_t szWatermark[128];
	static bool bGotWatermark = false;

	if (!bGotWatermark)
	{
		if (!MultiByteToWideChar( CP_UTF8, 0, "xL4D2 by crack", -1, szWatermark, 128 ) > 0)
		{
			return;
		}

		bGotWatermark = true;
	}

	RenderManager::Text(200,100, COLOR_SLATEGRAY, m_ESPFont, szWatermark);

	//g_pInterfaces->Surface->DrawSetTextFont( m_ESPFont );
	//g_pInterfaces->Surface->DrawSetTextColor( 255, 0, 0, 255 );
	//g_pInterfaces->Surface->DrawSetTextPos( 200, 200 );
	//g_pInterfaces->Surface->DrawPrintText( szWatermark, wcslen( m_szWatermark ) );
}


void CESP::DrawName(const char* text, Box size, CBaseEntity* pEntity)
{
	RECT nameSize = RenderManager::GetTextSize(m_ESPFont, text);

	RenderManager::Text(
		size.x + (size.w / 2) - (nameSize.right / 2), 
		size.y - 13 /*11*/, 
		COLOR_WHITE,
		m_ESPFont,
		text);
}

void CESP::DrawRainbowBox(Box box)
{
	RenderManager::Outline(box.x, box.y, box.w, box.h, m_RainbowColor);
	RenderManager::Outline(box.x - 1, box.y - 1, box.w + 2, box.h + 2, m_RainbowColor);
	RenderManager::Outline(box.x + 1, box.y + 1, box.w - 2, box.h - 2, m_RainbowColor);
	RenderManager::Outline(box.x - 1, box.y - 1, box.w + 2, box.h + 2, m_RainbowColor);
	RenderManager::Outline(box.x + 1, box.y + 1, box.w - 2, box.h - 2, m_RainbowColor);
}

void CESP::DrawESPBox(Box box, const Color color)
{
	RenderManager::Outline(box.x, box.y, box.w, box.h, color);
	RenderManager::Outline(box.x - 1, box.y - 1, box.w + 2, box.h + 2, color);
	RenderManager::Outline(box.x + 1, box.y + 1, box.w - 2, box.h - 2, color);
	RenderManager::Outline(box.x - 1, box.y - 1, box.w + 2, box.h + 2, color);
	RenderManager::Outline(box.x + 1, box.y + 1, box.w - 2, box.h - 2, color);
}

void CESP::DrawPlayer( int entIndex )
{
	Box box = { -1, -1, -1, -1 };

	CBaseEntity* pEntity = g_pInterfaces->EntityList->GetClientEntity(entIndex);

	Vector max = pEntity->GetIClientUnknownOther()->GetCollideable()->OBBMaxs();

	Vector pos, pos3D;
	Vector top, top3D;

	pos3D = pEntity->GetOrigin();
	top3D = pos3D + Vector(0, 0, max.z);

	if(!g_pInterfaces->EngineClient->IsConnected() || !g_pInterfaces->EngineClient->IsInGame())
		return;

	if (!WorldToScreen(pos3D, pos) || !WorldToScreen(top3D, top))
		return;

	pLocal = g_pInterfaces->EntityList->GetClientEntity( g_pInterfaces->EngineClient->GetLocalPlayer() );

	if (pLocal == nullptr)
		return;

	L4D2ClassID nClassID = (L4D2ClassID)pEntity->GetClientClass()->m_ClassID;

	if(GetBox(pEntity, box, true))
	{
		if(pEntity->GetGroup() != GROUP::GROUP_PLAYER)
		{
			DrawSkeleton(entIndex);
			switch( nClassID )
			{
				case L4D2ClassID::Spitter:
					DrawESPBox(box, COLOR_CHARTREUSE);
					break;
				case L4D2ClassID::Jockey:
					DrawESPBox(box, COLOR_DODGERBLUE);
					break;
				case L4D2ClassID::Charger:
					DrawESPBox(box, COLOR_GOLD);
					break;
				case L4D2ClassID::Hunter:
					DrawESPBox(box, COLOR_PURPLE);
					break;
				case L4D2ClassID::Smoker:
					DrawESPBox(box, COLOR_CHOCOLATE);
					break;
				case L4D2ClassID::Boomer:
					DrawESPBox(box, COLOR_YELLOWGREEN);
					break;
				case L4D2ClassID::Witch:
					DrawESPBox(box, COLOR_SALMON);
					break;
				case L4D2ClassID::Tank:
					DrawESPBox(box, COLOR_RED);
					break;
				case L4D2ClassID::Infected:
					DrawESPBox(box, COLOR_LINEN);
					break;
				default:
					DrawESPBox(box, COLOR_BLACK);
					break;
			};
		}

		if (nClassID == L4D2ClassID::Infected)
		{
			char buffer[128];
			sprintf_s( buffer, sizeof( buffer ), "%s", "Infected");
			DrawName(buffer, box, pEntity);
		}
		else if (pEntity->GetGroup() == GROUP::GROUP_PLAYER)
		{
			player_info_t playerInfo;

			if(g_pInterfaces->EngineClient->GetPlayerInfo(entIndex, &playerInfo))
			{
				char buffer[128];
				sprintf_s( buffer, sizeof( buffer ), "%s (%d)", playerInfo.name, pEntity->GetHealth() );
				DrawName(buffer, box, pEntity);
			}
			else
			{
				char buffer[128];
				sprintf_s( buffer, sizeof( buffer ), "%s (%d)", "player", pEntity->GetHealth() );
				DrawName(buffer, box, pEntity);
			}				
		}
		else
		{
			char buffer[128];
			sprintf_s( buffer, sizeof( buffer ), "%s (%d)", pEntity->GetClientClass()->m_pNetworkName, pEntity->GetHealth() );
			DrawName(buffer, box, pEntity);
		}
	}
}

void CESP::DrawDrop( int entIndex )
{

}

void CESP::DrawSkeleton( int entIndex )
{
	Vector vParent, vChild, sParent, sChild;
	static matrix3x4 matrix[128];

	CBaseEntity* pEntity = g_pInterfaces->EntityList->GetClientEntity(entIndex);
	

	studiohdr_t* StudioModel = g_pInterfaces->ModelInfo->GetStudioModel( pEntity->GetModel() );
	
	if (!StudioModel)
		return;

	if (!pEntity->SetupBones( matrix, 128, 0x100, g_pInterfaces->GlobalVars->curtime ))
		return;

	g_pInterfaces->Surface->DrawSetColor( Color(255, 255, 255, 255 ));

	for (int i = 0; i < StudioModel->numbones; i++)
	{
		auto m_Bone = StudioModel->GetBone( i );
		if (!m_Bone || !(m_Bone->flags & 256) || m_Bone->parent == -1)
			continue;

		Vector vecBonePos;
		if (!WorldToScreen( Vector( matrix[i][0][3], matrix[i][1][3], matrix[i][2][3] ), vecBonePos ))
			continue;

		Vector vecBoneParent;
		if (!WorldToScreen( Vector( matrix[m_Bone->parent][0][3], matrix[m_Bone->parent][1][3], matrix[m_Bone->parent][2][3] ), vecBoneParent ))
			continue;

		g_pInterfaces->Surface->DrawLine( vecBonePos.x, vecBonePos.y, vecBoneParent.x, vecBoneParent.y );
	}
	
	/*
	for (int j = 0; j < StudioModel->numbones; j++)
	{
		mstudiobone_t* pBone = StudioModel->GetBone(j);
		if (pBone && (pBone->flags & BONE_USED_BY_HITBOX) && (pBone->parent != -1))
		{
			vChild = pEntity->GetBonePos(j, g_pInterfaces->GlobalVars->curtime);
			vParent = pEntity->GetBonePos(pBone->parent, g_pInterfaces->GlobalVars->curtime);
			if (RenderManager::TransformScreen(vParent, sParent) && 
					RenderManager::TransformScreen(vChild, sChild))
			{
				RenderManager::Line(sParent[0], sParent[1], sChild[0], sChild[1], Color(0, 255, 0));
			}
		}
	}*/
}

float dot_product_t(const float* a, const float* b) 
{
	return (a[0] * b[0] + a[1] * b[1] + a[2] * b[2]);
}
void vector_transform_a(const float *in1, const matrix3x4& in2, float *out) 
{
	out[0] = dot_product_t(in1, in2[0]) + in2[0][3];
	out[1] = dot_product_t(in1, in2[1]) + in2[1][3];
	out[2] = dot_product_t(in1, in2[2]) + in2[2][3];
}
inline void vector_transform_z(const Vector& in1, const matrix3x4 &in2, Vector &out) 
{
	vector_transform_a(&in1.x, in2, &out.x);
}

bool CESP::GetBox(CBaseEntity* pEntity, Box& box, bool dynamic)
{
	DWORD m_rgflCoordinateFrame = (DWORD)0x344; //(DWORD)0x374 - 0x30
	const matrix3x4& trnsf = *(matrix3x4*)((DWORD)pEntity + (DWORD)m_rgflCoordinateFrame);

	Vector  vOrigin, min, max, sMin, sMax, sOrigin,
		flb, brt, blb, frt, frb, brb, blt, flt;
	float left, top, right, bottom;

	vOrigin = pEntity->GetOrigin();
	min = pEntity->GetCollisionProperty()->GetMins();
	max = pEntity->GetCollisionProperty()->GetMaxs();

	if (!dynamic) 
	{
		min += vOrigin;
		max += vOrigin;
	}

	Vector points[] = 
	{ 
		Vector(min.x, min.y, min.z),
		Vector(min.x, max.y, min.z),
		Vector(max.x, max.y, min.z),
		Vector(max.x, min.y, min.z),
		Vector(max.x, max.y, max.z),
		Vector(min.x, max.y, max.z),
		Vector(min.x, min.y, max.z),
		Vector(max.x, min.y, max.z) 
	};

	Vector vector_transformed[8];

	if (dynamic)
	{
		for (int i = 0; i < 8; i++)
		{
			vector_transform_z(points[i], trnsf, vector_transformed[i]);
			points[i] = vector_transformed[i];
		}
	}
	if (!RenderManager::TransformScreen(points[3], flb) || !RenderManager::TransformScreen(points[5], brt)
		|| !RenderManager::TransformScreen(points[0], blb) || !RenderManager::TransformScreen(points[4], frt)
		|| !RenderManager::TransformScreen(points[2], frb) || !RenderManager::TransformScreen(points[1], brb)
		|| !RenderManager::TransformScreen(points[6], blt) || !RenderManager::TransformScreen(points[7], flt))
		return false;

	Vector arr[] = 
	{ 
		flb, 
		brt, 
		blb, 
		frt, 
		frb, 
		brb, 
		blt, 
		flt 
	};

	left = flb.x;
	top = flb.y;
	right = flb.x;
	bottom = flb.y;

	for (int i = 1; i < 8; i++) 
	{
		if (left > arr[i].x)
			left = arr[i].x;
		if (bottom < arr[i].y)
			bottom = arr[i].y;
		if (right < arr[i].x)
			right = arr[i].x;
		if (top > arr[i].y)
			top = arr[i].y;
	}

	box.x = left;
	box.y = top;
	box.w = right - left;
	box.h = bottom - top;

	return true;
}