#ifndef ESP_H
#define ESP
#pragma once


#include "Entity.h"
#include "Interfaces.h"
#include "MathUtils.h"
#include "EngineUtils.h"
#include "RenderManager.h"
#include "L4D2ClassID.h"
#include "IXDrawable.h"
#include "RGB.h"

#define ESP_WORLD_MAX_RANGE 2000.0f

struct Box
{
	int x, y, w, h;
};

namespace VESP
{
	class CESP
	{
	public:
		CESP();
		~CESP() {}
		
	public:
		void Draw();
		bool Initialize();

	private:
		CBaseEntity* pLocal;
		HFont m_ESPFont;
		wchar_t* m_szWatermark;
		bool m_bInit;
		struct ESPBox
		{
			int x, y, w, h;
		};
		
		int m_rainbowR, m_rainbowG, m_rainbowB;

		Color m_RainbowColor;
		
	private:
		void DrawWatermark();;
		void DrawDrop( int entIndex );
	private:
		void DrawPlayer( int entIndex );
		void DrawRainbowBox(Box box);
		void DrawESPBox(Box box, const Color color);
		void DrawName(const char* text, Box size, CBaseEntity* pEntity);
		void DrawSkeleton( int entIndex );
		void DrawOutline(int x, int y, int w, int h, BYTE r, BYTE g, BYTE b, BYTE a);
		bool GetBox(CBaseEntity* pEntity, Box& box, bool dynamic);
		void UpdateRainbow();
	};
}

#endif //ESP_H