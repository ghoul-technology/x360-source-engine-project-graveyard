#include "EngineUtils.h"

using namespace EngineUtils;

bool EngineUtils::IsVisible(CBaseEntity* pLocal, CBaseEntity* pEntity, int BoneID)
{
	if (BoneID < 0) return false;

	trace_t Trace;
	Vector start = pLocal->GetOrigin() + pLocal->GetViewOffset();
	Vector end = GetHitboxPosition(pEntity, BoneID);

	TraceLine(start, end, MASK_SOLID, pLocal, 0, &Trace);

	if (Trace.m_pEnt == pEntity)
	{
		return true;
	}

	if (Trace.fraction == 1.0f)
	{
		return true;
	}

	return false;
}

void EngineUtils::TraceLine(const Vector& vecAbsStart, const Vector& vecAbsEnd, unsigned int mask,
	const IClientEntity *ignore, int collisionGroup, trace_t *ptr)
{
	Ray_t ray;
	ray.Init(vecAbsStart, vecAbsEnd);

	CTraceFilterSimple filter(ignore, collisionGroup);

	g_pInterfaces->Trace->TraceRay( ray, mask, &filter, ptr );
}

Vector EngineUtils::GetHitboxPosition(CBaseEntity* obj, int Hitbox)
{
	matrix3x4 bone_matrix[128];
	if (obj->SetupBones(bone_matrix, 128, 0x00000100, obj->GetSimulationTime())) {
		if (obj->GetModel())
		{
			auto studio_model = g_pInterfaces->ModelInfo->GetStudioModel(obj->GetModel());

			if (studio_model)
			{
				auto hitbox = studio_model->GetHitboxSet(0)->GetHitbox(Hitbox);
				Vector Point[] =
				{
					Vector(hitbox->bbmin.x, hitbox->bbmin.y, hitbox->bbmin.z),
					Vector(hitbox->bbmin.x, hitbox->bbmax.y, hitbox->bbmin.z),
					Vector(hitbox->bbmax.x, hitbox->bbmax.y, hitbox->bbmin.z),
					Vector(hitbox->bbmax.x, hitbox->bbmin.y, hitbox->bbmin.z),
					Vector(hitbox->bbmax.x, hitbox->bbmax.y, hitbox->bbmax.z),
					Vector(hitbox->bbmin.x, hitbox->bbmax.y, hitbox->bbmax.z),
					Vector(hitbox->bbmin.x, hitbox->bbmin.y, hitbox->bbmax.z),
					Vector(hitbox->bbmax.x, hitbox->bbmin.y, hitbox->bbmax.z)
				};
				Vector vMin, vMax, vCenter, sCenter;


				MathUtils::VectorTransform(hitbox->bbmin, bone_matrix[hitbox->bone], vMin);
				MathUtils::VectorTransform(hitbox->bbmax, bone_matrix[hitbox->bone], vMax);

				vCenter = ((vMin + vMax) *0.5f);
				int iCount = 7;

				for (int i = 0; i <= iCount; i++)
				{
					Vector vTargetPos;
					switch (i)
					{
						case 0:
						default:
							vTargetPos = vCenter; break;

						case 1:
							vTargetPos = (Point[7] + Point[1]);
							break;
						case 2:
							vTargetPos = (Point[3] + Point[4]);
							break;
						case 3:
							vTargetPos = (Point[4] + Point[0]);
							break;
						case 4:
							vTargetPos = (Point[2] + Point[7]) * (35 / 100);
							break; 
						case 5:
							vTargetPos = (Point[4] + Point[0]);
							break;
						case 6:
							vTargetPos = (Point[5] + Point[3]) * 0.6; 
							break;
						case 7:
							vTargetPos = (Point[1] + Point[2]) * 0.6; 
							break;
					}

					return vTargetPos;
				}
			}
		}
	}

	return Vector();
}

uint8_t EngineUtils::GetHeadHitboxID( const uint32_t& nClassID)
{
	switch (nClassID)
	{
	case L4D2ClassID::Infected:
		return 15;
		break;

	case L4D2ClassID::Tank:
		return 12;
		break;

	case L4D2ClassID::Spitter:
		return 4;
		break;

	case L4D2ClassID::Jockey:
		return 4;
		break;

	case L4D2ClassID::Charger:
		return 9;

	default:
		return 10;
		break;
	}
}

Vector EngineUtils::GetHitBox( CBaseEntity * pEntity, int8_t nHitbox )
{
	matrix3x4 matrix[128];

	if (pEntity->SetupBones( matrix, 128, 0x100, 0.f ))
	{
		studiohdr_t* hdr = g_pInterfaces->ModelInfo->GetStudioModel( pEntity->GetModel() );
		mstudiohitboxset_t* set = hdr->GetHitboxSet( 0 );

		mstudiobbox_t* hitbox = set->GetHitbox( nHitbox );

		if (hitbox)
		{
			Vector vMin, vMax, vCenter;
			MathUtils::VectorTransform( hitbox->bbmin, matrix[hitbox->bone], vMin );
			MathUtils::VectorTransform( hitbox->bbmax, matrix[hitbox->bone], vMax );

			
			vCenter = ((vMin + vMax) * 0.5f);

			return vCenter;
		}

	}

	return Vector( 0,0,0 );
}