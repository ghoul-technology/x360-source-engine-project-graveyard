#ifndef ENGINEUTILS_H
#define ENGINEUTILS_H

#include "Entity.h"
#include "Interfaces.h"
#include "MathUtils.h"
#include "L4D2ClassID.h"
#include <stdint.h>

namespace EngineUtils
{
	bool IsVisible(CBaseEntity* pLocal, CBaseEntity* pEntity, int BoneID);

	void TraceLine(const Vector& vecAbsStart, const Vector& vecAbsEnd, unsigned int mask,
		const IClientEntity *ignore, int collisionGroup, trace_t *ptr);

	Vector GetHitboxPosition(CBaseEntity* obj, int Hitbox);

	Vector GetHitBox(CBaseEntity* pEntity, int8_t nHitbox);

	uint8_t GetHeadHitboxID( const uint32_t & nClassID );

}

#endif //ENGINEUTILS_H