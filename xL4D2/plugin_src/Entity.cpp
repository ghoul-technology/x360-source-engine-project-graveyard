#include "Entity.h"

Vector CBaseEntity::GetBonePos(int i, float curTime)
{
	matrix3x4 boneMatrix[128];
	if (this->SetupBones(boneMatrix, 128, BONE_USED_BY_HITBOX, curTime))
	{
		return Vector(boneMatrix[i][0][3], boneMatrix[i][1][3], boneMatrix[i][2][3]);
	}
	return Vector(0, 0, 0);
}

const GROUP CBaseEntity::GetGroup()
{
	// Tank, Witch
	const DWORD m_Boss[] = { 274, 275 };
	// Boomer, Charger, Smoker, Hunter, Jockey, Spitter
	const DWORD m_Special[] = { 0, 99, 268, 261, 263, 270 };
	// Infected
	const DWORD m_Infected = 262;
	// CTerrorPlayer, SurvivorBot
	const DWORD m_Player[] = { 23, 273 };

	DWORD m_ClassID = this->GetClientClass()->m_ClassID;

	if (m_ClassID == m_Infected)
		return GROUP_INFECTED;

	else if (std::find( std::begin( m_Special ), std::end( m_Special ), m_ClassID ) != std::end( m_Special ))
		return GROUP_SPECIAL;

	else if (std::find( std::begin( m_Player ), std::end( m_Player ), m_ClassID ) != std::end( m_Player ))
		return GROUP_PLAYER;

	else if (std::find( std::begin( m_Boss ), std::end( m_Boss ), m_ClassID ) != std::end( m_Boss ))
		return GROUP_BOSS;

	return GROUP_INVALID;
}

const bool CBaseEntity::ValidEntity()
{
	if (this->IsDormant())
		return false;

	const auto nTeam = this->GetTeam();

	if (nTeam != 2 && nTeam != 3)
		return false;

	auto m_Group = this->GetGroup();
	auto m_Sequence = this->GetSequence();

	IClientUnknownOther* pClientUnk = this->GetIClientUnknownOther();

	ICollideable* pColl = pClientUnk->GetCollideable();

	auto m_SolidFlags = pColl->GetSolidFlags();

	if (m_Group == GROUP_INVALID)
		return false;

	if (m_Group == GROUP_BOSS)
	{
		if (m_SolidFlags & 4)
			return false;

		if (m_Sequence > 70)
			return false;
	}

	else if (m_Group == GROUP_SPECIAL)
	{
		if (m_SolidFlags & 4)
			return false;
	}

	else if (m_Group == GROUP_INFECTED)
	{
		if (m_SolidFlags & 4)
			return false;

		if (m_Sequence > 305)
			return false;
	}

	return true;
}

DWORD_PTR CBaseEntity::GetNetVarPtr(DWORD_PTR crc)
{
	return g_pNetVar->GetNetVar(crc);
}

DWORD_PTR ScriptCreatedItem::GetNetVarPtr(DWORD_PTR crc)
{
	return g_pNetVar->GetNetVar(crc);
}

DWORD_PTR AttributeContainer::GetNetVarPtr(DWORD_PTR crc)
{
	return g_pNetVar->GetNetVar(crc);
}

DWORD_PTR C_BaseCombatWeapon::GetNetVarPtr(DWORD_PTR crc)
{
	return g_pNetVar->GetNetVar(crc);
}

DWORD_PTR CLocalPlayerExclusive::GetNetVarPtr(DWORD_PTR crc)
{
	return g_pNetVar->GetNetVar(crc);
}

DWORD_PTR CollisionProperty::GetNetVarPtr(DWORD_PTR crc)
{
	return g_pNetVar->GetNetVar(crc);
}