#ifndef ENTITY_H
#define ENTITY_H

#pragma once
#include "EntityConsts.h"
#include <algorithm>
#include "IClientEntity.h"
#include "NetVar.h"

#include <xtl.h>
using namespace std;

#define BONE_USED_BY_HITBOX			0x00000100

#define GET_GLOABL_NETVAR(netvar_crc) 

#define dwThis (DWORD)this
#define NETVAR(type,offset) *(type*)(dwThis + offset)
#define CNETVAR(type,offset) (type)(dwThis + offset)

#define CNETVAR_FUNC(type,name,netvar_crc) \
	type name() \
	{ \
		static DWORD dwObserverTarget=this->GetNetVarPtr(netvar_crc); \
		return NETVAR(type,dwObserverTarget); \
	}

#define CPNETVAR_FUNC(type,name,netvar_crc) \
	type name() \
	{ \
		static DWORD dwObserverTarget=this->GetNetVarPtr(netvar_crc); \
		return CNETVAR(type,dwObserverTarget); \
	}

class CollisionProperty
{
public:
	CNETVAR_FUNC(Vector, GetMins, 0xF6F78BAB);//m_vecMins
	CNETVAR_FUNC(Vector, GetMaxs, 0xE47C6FC4);//m_vecMaxs
	CNETVAR_FUNC(unsigned char, GetSolidType, 0xB86722A1);//m_nSolidType
	CNETVAR_FUNC(unsigned short, GetSolidFlags, 0x63BB24C1);//m_usSolidFlags
	CNETVAR_FUNC(int, GetSurroundsType, 0xB677A0BB); //m_nSurroundType

	bool IsSolid()
	{
		return (GetSolidType() != FSOLID_NONE) && ((GetSolidFlags() & FSOLID_NOT_SOLID) == 0);
	}

private:
	DWORD_PTR GetNetVarPtr(DWORD_PTR crc);
};


class CLocalPlayerExclusive
{
public:
	CNETVAR_FUNC(Vector, GetViewPunchAngle, 0x68F014C0);//m_viewPunchAngle
	CNETVAR_FUNC(Vector, GetAimPunchAngle, 0xBF25C290);//m_aimPunchAngle
	CNETVAR_FUNC(Vector, GetAimPunchAngleVel, 0x8425E045);//m_aimPunchAngleVel

private:
	DWORD_PTR GetNetVarPtr(DWORD_PTR crc);
};

class ScriptCreatedItem
{
public:
	CPNETVAR_FUNC(short*, ItemDefinitionIndex, 0xE67AB3B8); //m_iItemDefinitionIndex
	CPNETVAR_FUNC(int*, ItemIDHigh, 0x714778A); //m_iItemIDHigh
	CPNETVAR_FUNC(int*, ItemIDLow, 0x3A3DFC74); //m_iItemIDLow
	CPNETVAR_FUNC(int*, AccountID, 0x24abbea8); //m_iAccountID

private:
	DWORD_PTR GetNetVarPtr(DWORD_PTR crc);
};

class AttributeContainer
{
public:
	CPNETVAR_FUNC(ScriptCreatedItem*, m_Item, 0x7E029CE5);

private:
	DWORD_PTR GetNetVarPtr(DWORD_PTR crc);
};

class CBaseEntity : public IClientEntity
{
public:
	CPNETVAR_FUNC(CLocalPlayerExclusive*, localPlayerExclusive, 0x7177BC3E);// m_Local

	CPNETVAR_FUNC(CollisionProperty*, GetCollisionProperty, 0xE477CBD0);//m_Collision

	CPNETVAR_FUNC(Vector*, GetOriginPtr, 0x1231CE10); //m_vecOrigin

	CNETVAR_FUNC(char, GetLifeState, 0xD795CCFC); //m_lifeState // unsigned char

	CNETVAR_FUNC(int, GetHealth, 0xA93054E3); //m_iHealth

	CNETVAR_FUNC(HANDLE, GetActiveWeaponHandle, 0xB4FECDA3); //m_hActiveWeapon

	CNETVAR_FUNC(int, GetTickBase, 0xD472B079); //m_nTickBase

	CNETVAR_FUNC(int, GetTeam, 0xC08B6C6E); //m_iTeamNum

	CNETVAR_FUNC(int, GetFlags, 0xE456D580); //m_fFlags

	CNETVAR_FUNC(Vector, GetOrigin, 0x1231CE10); //m_vecOrigin 0x0134

	CNETVAR_FUNC(Vector, GetViewOffset, 0xA9F74931); //m_vecViewOffset[0]

	CNETVAR_FUNC(Vector, GetPunch, 0xBF25C290);

	CNETVAR_FUNC(Vector, GetMins, 0xF6F78BAB);//m_vecMins

	CNETVAR_FUNC(Vector, GetMaxs, 0xE47C6FC4);//m_vecMaxs

	CNETVAR_FUNC(float, GetSimulationTime, 0xC4560E44);//m_flSimulationTime

	CNETVAR_FUNC(int, GetSequence, 0x85362B79); //seq

	inline const void SetTickBase( const int& tickbase ){ (*(MakePointerGEN< int >( this, 0x14bc ))) = tickbase; }

	inline const void SetFlags( const int& flags ){ (*MakePointerGEN<int>( this, 0xf0 )) = flags;}

	inline const Vector GetEyes(){ return GetOrigin() + GetViewOffset();}

	Vector GetBonePos(int i, float curTime);

	const GROUP GetGroup();

	const bool ValidEntity();

private:
	DWORD_PTR GetNetVarPtr(DWORD_PTR crc);
};

class C_BaseCombatWeapon
{
public:

	CNETVAR_FUNC(int, GetAmmoInClip, 0x97B6F70C); //m_fFlags
	CNETVAR_FUNC(HANDLE, GetOwnerHandle, 0xC32DF98D); //m_hOwner
	CNETVAR_FUNC(float, GetAccuracyPenalty, 0xE2958A63); //m_fAccuracyPenalty
	CNETVAR_FUNC(Vector, GetOrigin, 0x1231CE10); //m_vecOrigin
	CPNETVAR_FUNC(int*, FallbackPaintKit, 0xADE4C870);
	CPNETVAR_FUNC(int*, FallbackSeed, 0xC2D0683D); // m_nFallbackSeed
	CPNETVAR_FUNC(float*, FallbackWear, 0xA263576C); //m_flFallbackWear
	CPNETVAR_FUNC(int*, FallbackStatTrak, 0x1ED78768); //m_nFallbackStatTrak
	CPNETVAR_FUNC(int*, OwnerXuidLow, 0xAD8D897F);
	CPNETVAR_FUNC(int*, OwnerXuidHigh, 0x90511E77);
	CPNETVAR_FUNC(int*, ViewModelIndex, 0x7F7C89C1);
	CPNETVAR_FUNC(int*, ModelIndex, 0x27016F83);
	CPNETVAR_FUNC(int*, WorldModelIndex, 0x4D8AD9F3);
	CPNETVAR_FUNC(char*, szCustomName, 0x0);
	CPNETVAR_FUNC(AttributeContainer*, m_AttributeManager, 0xCFFCE089);

	float GetMaxSpread(){ return *(float*)(reinterpret_cast<uintptr_t>(this) + 0xD0C); }

	float NextPrimaryAttack() { return (*MakePointerGEN< float >( this, 0x150 )); }

	FileWeaponInfo_t* GetCSWpnData()
	{
		return GetFunction<FileWeaponInfo_t*(__thiscall*)(C_BaseCombatWeapon*)>( this, 13 )(this);
	}

	const char* WeaponAlias( const DWORD& nWeaponID )
	{
		static auto FnWeaponIDToAlias = (char*(*)(DWORD))(reinterpret_cast<PDWORD>(0x8849BE90));
		return FnWeaponIDToAlias( nWeaponID );
	}
	short* GetItemDefinitionIndex()
	{
		return (short*)m_AttributeManager()->m_Item()->ItemDefinitionIndex();
	}
private:
	DWORD_PTR GetNetVarPtr(DWORD_PTR crc);
};

#endif //ENTITY_H