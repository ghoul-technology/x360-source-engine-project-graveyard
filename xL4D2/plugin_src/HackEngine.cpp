#include "HackEngine.h"

std::unique_ptr<CHackEngine>*	g_pHackEngine;

VOID StartEngine()
{
	DbgPrint("Loading xL4D2...\n");
	// Inst. interfaces class to hold the interface references
	DbgPrint("Loading interfaces...\n");
	g_pInterfaces = new CInterfaces();
	DbgPrint("Loading developer console emulation...\n");
	RECT viewPort = RenderManager::GetViewport();
	DbgPrint("Viewport Size = T:%i B:%i L:%i R:%i\n", viewPort.top, viewPort.bottom, viewPort.left, viewPort.right);

	g_pDevConsole = new CDevConsole();
	DbgPrint("Loading netvars...\n");
	g_pNetVar = new CNetVar();
	DbgPrint("Loading hack engine...\n");
	g_pHackEngine = new std::unique_ptr<CHackEngine>(new CHackEngine());
}

float PatchedGetRoundDuration(void* thisPtr, int unk1)
{
	return 99940000.0f;
}

CHackEngine::CHackEngine()
{
	this->RunHooks();
}


void CHackEngine::RunHooks()
{
	DbgPrint("Running hooks...\n");
	DbgPrint("Hooking developer console output...\n");
	// hook console spew
	g_pDevConsole->HookSpew((DWORD*)printf);
	DbgPrint("Hooking engine functions...\n");

	// Run hooks.
	Hooks::Initialize();
}

void CHackEngine::RemoveHooks()
{

}