#ifndef HACKENGINE_H
#define HACKENGINE_H

#pragma once

#include "common.h"
#include "Interfaces.h"
#include "DevConsole.h"
#include "Hooks.h"
#include "ESP.h"
#include "VMTHook.h"

class CHackEngine
{
public:
	CHackEngine();
	~CHackEngine();

public:
	void Start();
	void Stop();
	void Restart();

private:
	void RunHooks();
	void RemoveHooks();

};

extern std::unique_ptr<CHackEngine>* g_pHackEngine;

// begin.
VOID StartEngine();

#endif //HACKENGINE_H