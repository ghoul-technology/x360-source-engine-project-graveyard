#include "Hooks.h"

CVMTHook*		g_pClientModeHook;
CVMTHook*		g_pPaintHook;
CVMTHook*		g_pRenderViewHook;
CVMTHook*		g_pGameEventMgrHook;
CVMTHook*		g_pTerrorGameRulesHook;
IMaterial*		g_pMaterial;
void Hooks::Initialize()
{
	int localPlayer = 0;
	player_info_t sPlayerInfo;

	while(!(g_pInterfaces->EngineClient->IsInGame() && g_pInterfaces->EngineClient->IsConnected()))
	{
		DbgPrint("Waiting for game to start for hooks to initialize. Waiting 10 seconds.\n");
		Sleep(10000);
	}

	g_pMaterial = g_pInterfaces->MaterialSystem->FindMaterial( "debug/debugambientcube", "Model textures" );
	//g_pMaterial->SetMaterialVarFlag( MATERIAL_VAR_IGNOREZ, true );
	//g_pMaterial->ColorModuleate( 1.f, 1.f, 1.f );

	// Hook EngineVGui::Paint()
	g_pPaintHook = new CVMTHook();

	if(!g_pPaintHook->bInitialize((PDWORD*)g_pInterfaces->EngineVGui))
		DbgPrint("Failed to initialize hook for EngineVGui!\n");
	else
	{
		if(!g_pPaintHook->HookMethod((DWORD)Paint, 14))
		{
			DbgPrint("Failed to hook EngineVGui::Paint()! (index 14)\n");
		}
		else
		{
			DbgPrint("Successfully hooked EngineVGui::Paint()\n");
		}
	}

	g_pClientModeHook = new CVMTHook();

	if(!g_pClientModeHook->bInitialize((PDWORD*)0x88821508))
		DbgPrint("Failed to hook client hook!\n");
	else
	{
		if(!g_pClientModeHook->HookMethod((DWORD)CreateMove, 27))
		{
			DbgPrint("Failed to hook CClientModeShared::Move(float,CClientCmd)! (index 27)\n");
		}
		else
		{
			DbgPrint("Successfully hooked CClientModeShared::Move(float,CClientCmd)! (index 27)\n");
		}
	}

	g_pRenderViewHook = new CVMTHook();

	if(!g_pRenderViewHook->bInitialize((PDWORD*)g_pInterfaces->RenderView))
		DbgPrint("Failed to hook render view hook!\n");
	else
	{
		if(!g_pRenderViewHook->HookMethod((DWORD)SceneEnd, 9))
		{
			DbgPrint("Failed to hook IRenderView::SceneEnd()! (index 9)\n");
		}
		else
		{
			DbgPrint("Successfully hooked IRenderView::SceneEnd()! (index 9)\n");
		}
	}

	g_pGameEventMgrHook = new CVMTHook();
	if(!g_pGameEventMgrHook->bInitialize((PDWORD*)g_pInterfaces->EventManager))
		DbgPrint("Failed to hook game event manager2!\n");
	else
	{
		if(!g_pGameEventMgrHook->HookMethod((DWORD)FireEventClientSide, 8))
		{
			DbgPrint("Failed to hook IGameEventManager2::FireEvent()! (index 8)\n");
		}
		else
		{
			DbgPrint("Successfully hooked IGameEventManager2::FireEvent()! (index 8)\n");
		}
	}

	//g_pTerrorGameRulesHook = new CVMTHook();
	localPlayer = g_pInterfaces->EngineClient->GetLocalPlayer();
	g_pInterfaces->EngineClient->GetPlayerInfo(localPlayer, &sPlayerInfo);
}

void __stdcall Hooks::Paint(void* thisptr, PaintMode_t mode)
{
	static auto StartDrawing = reinterpret_cast<void(__thiscall*)(ISurface*)>(0x84E2B450);
	static auto FinishDrawing = reinterpret_cast<void(__thiscall*)(ISurface*)>(0x84E2B710);

	static auto EnginePaint = reinterpret_cast<Hooks::fnPaint>(g_pPaintHook->GetOldVMT()[14]);
	EnginePaint(g_pInterfaces->EngineVGui, mode);

	static VESP::CESP* pESP = nullptr;

	if(!pESP)
	{
		pESP = new VESP::CESP();
		pESP->Initialize();
	}

	if (mode & PaintMode_t::PAINT_INGAMEPANELS) 
	{
		StartDrawing(g_pInterfaces->Surface);
		pESP->Draw();
		FinishDrawing(g_pInterfaces->Surface);
	}

}

void __fastcall Hooks::SceneEnd( void* thisptr, void* edx )
{
	static auto OSceneEnd = reinterpret_cast<Hooks::fnSceneEnd>(g_pRenderViewHook->GetOldVMT()[9]);
	OSceneEnd(edx);

	CBaseEntity* pLocal = reinterpret_cast<CBaseEntity*>(g_pInterfaces->EntityList->GetClientEntity( g_pInterfaces->EngineClient->GetLocalPlayer() ));

	if (!pLocal)
		return;	
	for (DWORD nIndex = 1; nIndex < g_pInterfaces->EntityList->GetHighestEntityIndex(); nIndex++)
	{
		auto pEntity = reinterpret_cast<CBaseEntity*>(g_pInterfaces->EntityList->GetClientEntity( nIndex ));
		if (!pEntity || pEntity == pLocal || !pEntity->ValidEntity())
			continue;

		g_pInterfaces->ModelRenderer->ForcedMaterialOverride( g_pMaterial );
		pEntity->DrawModel( 0x1 );
		g_pInterfaces->ModelRenderer->ForcedMaterialOverride( nullptr );

	}
}

bool __stdcall Hooks::CreateMove(void* thisptr, float flInputSampleTime, CUserCmd *cmd)
{
	static auto CLCreateMove = reinterpret_cast<Hooks::fnCreateMove>(g_pClientModeHook->GetOldVMT()[27]);

	CLCreateMove(thisptr, flInputSampleTime, cmd);

	//cmd->random_seed = MD5_PseudoRandom( cmd->command_number ) & 0x7fffffff;

	//static CCrackBot* pAimBot = nullptr;

	//if(!pAimBot)
	//{
		//pAimBot = new CCrackBot();
	//}

	//pAimBot->Execute(cmd);
	
	return true;
}

bool __stdcall Hooks::FireEventClientSide(void* thisptr, IGameEvent* eventFired)
{
	static auto GEMFireEvent = reinterpret_cast<Hooks::fnFireEventClientSide>(g_pGameEventMgrHook->GetOldVMT()[8]);

	DbgPrint("[GEM]Event fired name = %s value = %s\n", eventFired->GetName(), eventFired->GetString());

	return GEMFireEvent(thisptr, eventFired);
}
