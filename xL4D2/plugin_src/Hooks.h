#ifndef HOOKS_H
#define HOOKS_H
#pragma once


#include "IPanel.h"
#include "Interfaces.h"
#include "VMTHook.h"
#include "ESP.h"
#include "CrackBot.h"
#include "MD5.h"

namespace Hooks
{
	typedef void*	(*fnSceneEnd)(void*);
	typedef bool*	(*fnCreateMove)(void*, float, CUserCmd*);
	typedef void*	(__thiscall *fnPaint) (IEngineVGui*, PaintMode_t);
	typedef bool	(*fnFireEventClientSide)(void*,IGameEvent*);
	typedef DWORD		(*fnGetRoundDuration)(void*, int);
	extern void Initialize();
	extern void Unhook();

	extern void __fastcall SceneEnd( void* thisptr, void* edx );
	extern bool __stdcall CreateMove(void* thisptr, float flInputSampleTime, CUserCmd *cmd);
	extern void __stdcall Paint(void* thisptr, PaintMode_t mode);
	extern bool __stdcall FireEventClientSide(void* thisptr, IGameEvent* gameEvent);
	extern DWORD __stdcall GetRoundDuration(void* thisptr, int unk1);
};


#endif // HOOKS_H

