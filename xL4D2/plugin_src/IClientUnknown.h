#ifndef ICLIENTUNKNOWN_H
#define ICLIENTUNKNOWN_H

#pragma once

#include "IClientNetworkable.h"
#include "IClientRenderable.h"
#include "IHandleEntity.h"
#include "ICollideable.h"

class IClientUnknownOther : public IHandleEntity
{
	public:
		virtual ICollideable*		GetCollideable() = 0;
		virtual IClientNetworkable*	GetClientNetworkable() = 0;
		virtual IClientRenderable*	GetClientRenderable() = 0;
		virtual void*		GetIClientEntity() = 0;
		virtual void*				GetBaseEntity() = 0;
		virtual void*				GetClientThinkable() = 0;
};

class IClientUnknown
{
public:
	virtual ICollideable*		GetCollideable() = 0;
	virtual IClientNetworkable*	GetClientNetworkable() = 0;
	virtual IClientRenderable*	GetClientRenderable() = 0;
	virtual void*		GetIClientEntity() = 0;
	virtual void*				GetBaseEntity() = 0;
	virtual void*				GetClientThinkable() = 0;
};

#endif // ICLIENTUNKNOWN_H