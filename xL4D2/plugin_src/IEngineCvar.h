#ifndef IENGINECVAR_H
#define IENGINECVAR_H

#pragma once


#include "ConVar.h"

class IEngineCvar
{
public:
	ConVar* FindVar(const char* name)
	{
		return GetFunction<ConVar*(__thiscall*)(IEngineCvar*, const char*)>( this, 13 )(this, name);
	}
};

#endif // IENGINECVAR_H