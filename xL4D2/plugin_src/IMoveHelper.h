#ifndef IMOVEHELPER_H
#define IMOVEHELPER_H

#include "Entity.h"

class IMoveHelper
{
private:
	virtual void UnknownVirtual0() = 0;
public:
	virtual void SetHost( CBaseEntity* host ) = 0;
};

#endif //IMOVEHELPER_H