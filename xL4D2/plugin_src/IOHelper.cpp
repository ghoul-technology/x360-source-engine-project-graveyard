#include "IOHelper.h"

CIOHelper::CIOHelper()
{
	// mount base directory
	this->MountPath(APPMOUNTAS, "\\Device\\Harddisk0\\Partition1\\", TRUE);
}

BOOL CIOHelper::FileExists(PCHAR path)
{
	return FALSE;
}

BOOL CIOHelper::DriveExists(PCHAR drive)
{
	return FALSE;
}

HRESULT CIOHelper::MountPath(const char* szDrive, const char* szDevice, BOOL both)
{
	HRESULT res = -1;
	if(both)
	{
		res = this->_mountPath(szDrive, szDevice, SYS_STRING);
		res |= this->_mountPath(szDrive, szDevice, USR_STRING);
	}
	else
	{
		if(KeGetCurrentProcessType() == SYSTEM_PROC)
			res = this->_mountPath(szDrive, szDevice, SYS_STRING);
		else
			res = this->_mountPath(szDrive, szDevice, USR_STRING);
	}
	return res;
}

HRESULT CIOHelper::UnmountPath(const char* szDrive, BOOL both)
{
	HRESULT res = -1;
	if(both)
	{
		res = this->_unmountPath(szDrive, SYS_STRING);
		res |= this->_unmountPath(szDrive, USR_STRING);
	}
	else
	{
		if(KeGetCurrentProcessType() == SYSTEM_PROC)
			res = this->_unmountPath(szDrive, SYS_STRING);
		else
			res = this->_unmountPath(szDrive, USR_STRING);
	}
	return res;
}

HRESULT CIOHelper::_mountPath(const char* szDrive, const char* szDevice, 
	const char* sysStr)
{
	PCHAR szSysStr = NULL;
	if ( KeGetCurrentProcessType() == SYSTEM_PROC )
		szSysStr = "\\System??\\%s";
	else
		szSysStr = "\\??\\%s";

	CHAR szDestinationDrive[ MAX_PATH ];
	sprintf_s( szDestinationDrive, MAX_PATH, szSysStr, szDrive );

	STRING strDeviceName = MAKE_STRING( szDevice );
	STRING strLinkName = MAKE_STRING( szDestinationDrive );

	ObDeleteSymbolicLink( &strLinkName );

	return ObCreateSymbolicLink( &strLinkName, &strDeviceName );
}

HRESULT CIOHelper::_unmountPath(const char* szDrive, const char* sysStr)
{
	PCHAR szSysStr = NULL;
	if ( KeGetCurrentProcessType() == SYSTEM_PROC )
		szSysStr = "\\System??\\%s";
	else
		szSysStr = "\\??\\%s";

	CHAR szDestinationDrive[ MAX_PATH ];
	sprintf_s( szDestinationDrive, MAX_PATH, szSysStr, szDrive );

	STRING strLinkName = MAKE_STRING( szDestinationDrive );

	return ObDeleteSymbolicLink( &strLinkName );
}