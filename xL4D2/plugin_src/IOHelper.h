#ifndef IOHELPER_H
#define IOHELPER_H

#pragma once

#include "common.h"

#define APPMOUNTAS "XGP:"

#define SYS_STRING "\\System??\\%s"
#define USR_STRING "\\??\\%s"

#define XL4D2_ROOT_DIR = "\\Device\\Harddisk0\\Partition1\\"

class CIOHelper
{
public:
	CIOHelper();
	~CIOHelper();

	BOOL FileExists(PCHAR path);
	BOOL DriveExists(PCHAR drive);

	HRESULT MountPath(const char* drive, const char* device, BOOL both);
	HRESULT UnmountPath(const char* szDrive, BOOL both);

private:
	HRESULT _mountPath(const char* szDrive, const char* szDevice, const char* sysStr);
	HRESULT _unmountPath(const char* szDrive, const char* sysStr);

};

#endif //IOHELPER_H