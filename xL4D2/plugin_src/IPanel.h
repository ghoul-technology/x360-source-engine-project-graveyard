#ifndef IPANEL_H
#define IPANEL_H
#pragma once

#include "common.h"

class IPanel
{
	public:
		const char* GetName( DWORD iPanel )
		{
			return GetFunction<const char*(__thiscall*)(IPanel*, DWORD)>( this, 36 )(this, iPanel);
		}
};

#endif //IPANEL_H