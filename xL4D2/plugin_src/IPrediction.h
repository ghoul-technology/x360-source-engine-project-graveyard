#ifndef IPREDICTION_H
#define IPREDICTION_H
#pragma once


#include "IGameMovement.h"
#include "IMoveHelper.h"
#include "Entity.h"
#include "UserCmd.hpp"

class IPrediction
{
public:
	void RunCommand( CBaseEntity* player, CUserCmd* cmd, IMoveHelper* pHelper)
	{
		GetFunction<void( __thiscall* )(IPrediction*, CBaseEntity*, CUserCmd*, IMoveHelper*)>( this, 18 )(this, player, cmd, pHelper);
	}

	void SetupMove( CBaseEntity* player, CUserCmd* cmd, IMoveHelper* pHelper, CMoveData* move )
	{
		GetFunction<void( __thiscall* )(IPrediction*, CBaseEntity*, CUserCmd*, IMoveHelper*, CMoveData*)>( this, 19 )(this, player, cmd, pHelper, move);
	}

	void FinishMove( CBaseEntity* player, CUserCmd* cmd, CMoveData* move )
	{
		GetFunction<void( __thiscall* )(IPrediction*, CBaseEntity*, CUserCmd*, CMoveData*)>( this, 20 )(this, player, cmd, move);
	}
};

#endif //IPREDICTION_H