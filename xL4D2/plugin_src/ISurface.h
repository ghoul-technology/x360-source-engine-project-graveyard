#ifndef ISURFACE_H
#define ISURFACE_H
#pragma once


#include "Vector2D.h"
#include "Color.h"

struct Vertex_t
{
	Vector2D	m_Position;
	Vector2D	m_TexCoord;
	Vertex_t() {}
	Vertex_t(const Vector2D &pos, const Vector2D &coord = Vector2D(0, 0))
	{
		m_Position = pos;
		m_TexCoord = coord;
	}
	void Init(const Vector2D &pos, const Vector2D &coord = Vector2D(0, 0))
	{
		m_Position = pos;
		m_TexCoord = coord;
	}
};
typedef  Vertex_t FontVertex_t;

#define FW_DONTCARE         0
#define FW_THIN             100
#define FW_EXTRALIGHT       200
#define FW_LIGHT            300
#define FW_NORMAL           400
#define FW_MEDIUM           500
#define FW_SEMIBOLD         600
#define FW_BOLD             700
#define FW_EXTRABOLD        800
#define FW_HEAVY            900

enum FontFlags_t
{
	FONTFLAG_NONE,
	FONTFLAG_ITALIC = 0x001,
	FONTFLAG_UNDERLINE = 0x002,
	FONTFLAG_STRIKEOUT = 0x004,
	FONTFLAG_SYMBOL = 0x008,
	FONTFLAG_ANTIALIAS = 0x010,
	FONTFLAG_GAUSSIANBLUR = 0x020,
	FONTFLAG_ROTARY = 0x040,
	FONTFLAG_DROPSHADOW = 0x080,
	FONTFLAG_ADDITIVE = 0x100,
	FONTFLAG_OUTLINE = 0x200,
	FONTFLAG_CUSTOM = 0x400,
	FONTFLAG_BITMAP = 0x800,
};

enum FontDrawType_t
{
	FONT_DRAW_DEFAULT = 0,
	FONT_DRAW_NONADDITIVE,
	FONT_DRAW_ADDITIVE,
	FONT_DRAW_TYPE_COUNT = 2,
};

typedef unsigned long HFont;

class ISurface
{
public:
	inline void DrawSetColor( const Color color )
	{
		return GetFunction<void( __thiscall* )(ISurface*, Color)>( this, 10 )(this, color);
	}
	
	inline void DrawFilledRect(int x0, int y0, int x1, int y1)
	{
		return GetFunction<void( __thiscall* )(ISurface*, int, int, int, int)>( this, 12 )(this, x0, y0, x1, y1);
	}

	inline void DrawOutlinedRect( int x0, int y0, int x1, int y1 )
	{
		return GetFunction<void( __thiscall* )(ISurface*, int, int, int, int)>( this, 14 )(this, x0, y0, x1, y1);
	}

	inline void DrawLine( DWORD x0, DWORD y0, DWORD x1, DWORD y1 )
	{
		return GetFunction<void( __thiscall* )(ISurface*, DWORD, DWORD, DWORD, DWORD)>( this, 15 )(this, x0, y0, x1, y1);
	}

	inline void DrawSetTextFont( HFont font )
	{
		return GetFunction<void( __thiscall* )(ISurface*, HFont)>( this, 17 )(this, font);
	}

	inline void DrawSetTextColor( Color color )
	{
		int r, g, b, a;

		color.GetColor(r, g, b, a);

		return GetFunction<void( __thiscall* )(ISurface*, BYTE, BYTE, BYTE, BYTE)>( this, 19 )(this, r, g, b, a);
	}

	inline void DrawSetTextColor( BYTE r, BYTE g, BYTE b, BYTE a )
	{
		return GetFunction<void( __thiscall* )(ISurface*, BYTE, BYTE, BYTE, BYTE)>( this, 19 )(this, r, g, b, a);
	}

	inline void DrawSetTextPos( DWORD x, DWORD y )
	{
		return GetFunction<void( __thiscall* )(ISurface*, DWORD, DWORD)>( this, 20 )(this, x, y);
	}

	inline void DrawPrintText( const wchar_t* text, int textLen, FontDrawType_t DrawType = FONT_DRAW_DEFAULT )
	{
		return GetFunction<void( __thiscall* )(ISurface*, const wchar_t*, int, FontDrawType_t)>( this, 22 )(this, text, textLen, DrawType);
	}

	inline HFont Create_Font()
	{
		return GetFunction<HFont( __thiscall* )(ISurface*)>( this, 64 )(this);
	}

	inline bool SetFontGlyphSet( HFont font, const char* windowsFontName, int tall, int weight, int blur, int scanlines, int flags)
	{
		return GetFunction<bool( __thiscall* )(ISurface*, HFont, const char*, int, int, int, int, int)>( this, 65 )(this, font, windowsFontName, tall, weight, blur, scanlines, flags);
	}

	inline void GetTextSize(HFont font, const wchar_t *text, int &wide, int &tall)
	{
		return GetFunction<void( __thiscall* )(ISurface*, HFont, const wchar_t *text, int&, int&)>( this, 73 )(this, font, text, wide, tall);
	}

	inline void DrawOutlinedCircle(int x, int y, int r, int seg)
	{
		return GetFunction<void( __thiscall* )(ISurface*, int, int, int, int)>( this, 97 )(this, x, y, r, seg);
	}
};

#endif
