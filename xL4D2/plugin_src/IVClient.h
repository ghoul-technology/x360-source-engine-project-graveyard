#ifndef IVCLIENT_H
#define IVCLIENT_H

#pragma once


class IVClient
{
public:
	// Request a pointer to the list of client datatable classes (7)
	ClientClass* GetAllClasses()
	{
		return GetFunction<ClientClass*( __thiscall* )(IVClient*)>( this, 7 )(this);
	};

};

#endif //IVCLIENT_H