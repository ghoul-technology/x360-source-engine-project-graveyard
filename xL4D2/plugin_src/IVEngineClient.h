#ifndef IVENGINECLIENT_H
#define IVENGINECLIENT_H

#pragma once


#include "VMatrix.h"
#include "common.h"

#define VENGINE_CLIENT_INTERFACE_VERSION_13	"VEngineClient013"

class IVEngineClient
{
public:
	inline void GetScreenSize( int& w, int& h )
	{
		return GetFunction<void( __thiscall* )(IVEngineClient*, int&, int&)>( this, 5 )(this, w, h);
	}

	inline void ClientCmd( const char* cmd )
	{
		return GetFunction<void( __thiscall* )(IVEngineClient*, const char*)>( this, 7 )(this, cmd);
	}
	//8

	inline bool GetPlayerInfo(int ent_num, player_info_t *pinfo)
	{
		return GetFunction<bool( __thiscall* )(IVEngineClient*, int, player_info_t*)>( this, 8 )(this, ent_num, pinfo);
	}

	inline int const GetLocalPlayer()
	{
		return GetFunction<int( __thiscall* )(IVEngineClient*)>( this, 12 )(this);
	}

	inline void GetViewAngles( Vector& vAngles )
	{
		return GetFunction<void( __thiscall* )(IVEngineClient*, Vector&)>( this, 19 )(this, vAngles);
	}

	inline void SetViewAngles( Vector& vAngles )
	{
		return GetFunction<void( __thiscall* )(IVEngineClient*, Vector&)>( this, 20 )(this, vAngles);
	}

	inline bool IsInGame()
	{
		return GetFunction<bool( __thiscall* )(IVEngineClient*)>( this, 26 )(this);
	}

	inline bool IsConnected()
	{
		return GetFunction<bool( __thiscall* )(IVEngineClient*)>( this, 27 )(this);
	}

	const matrix3x4& WorldToScreenMatrix()
	{
		return GetFunction<matrix3x4&( __thiscall* )(IVEngineClient*)>( this, 37 )(this);
	}
private:

};
#endif //IVENGINECLIENT_H
