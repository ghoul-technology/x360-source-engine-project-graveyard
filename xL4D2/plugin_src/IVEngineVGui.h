#ifndef IVENGINEVGUI_H
#define IVENGINEVGUI_H
#pragma once



#define VENGINE_VGUI_INTERFACE_VERSION	"VEngineVGui001"

enum VGuiPanel_t
{
	PANEL_ROOT = 0,
	PANEL_GAMEUIDLL,
	PANEL_CLIENTDLL,
	PANEL_TOOLS,
	PANEL_INGAMESCREENS,
	PANEL_GAMEDLL,
	PANEL_CLIENTDLL_TOOLS
};

// In-game panels are cropped to the current engine viewport size
enum PaintMode_t
{
	PAINT_UIPANELS		= (1<<0),
	PAINT_INGAMEPANELS  = (1<<1),
	PAINT_CURSOR		= (1<<2), // software cursor, if appropriate
};

class IEngineVGui
{
public:
	inline DWORD GetPanel( VGuiPanel_t type )
	{
		return GetFunction<DWORD( __thiscall* )(IEngineVGui*, DWORD)>( this, 1 )(this, type);
	}
};

#endif // IVENGINEVGUI_H