#ifndef IXDRAWABLE_H
#define IXDRAWABLE_H

#include "common.h"

class IXDrawable
{
public:
	virtual void Draw() = 0;
};

#endif // IXDRAWABLE_H