#ifndef IXPANEL_H
#define IXPANEL_H

#pragma once

#include "common.h"
#include "Color.h"

class IXPanel : public IXDrawable
{
public:
	virtual RECT GetBounds() = 0;
	virtual void SetBounds(RECT newBounds) = 0;

	virtual Color GetColor() = 0;
	virtual void SetColor(Color newColor) = 0;
};

#endif //IXPANEL_H