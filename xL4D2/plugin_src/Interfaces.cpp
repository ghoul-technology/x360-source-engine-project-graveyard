#include "Interfaces.h"

CInterfaces* g_pInterfaces;

CInterfaces::CInterfaces(void)
{
	DbgPrint("Loading interfaces...");
	_initialize();
}


CInterfaces::~CInterfaces(void)
{
}


bool CInterfaces::_initialize()
{
	// TU6 export addresses
	CreateInterfaceFn client = (CreateInterfaceFn)0x8860E878;
	CreateInterfaceFn vgui2 = (CreateInterfaceFn)0x83C2C920;
	CreateInterfaceFn engine = (CreateInterfaceFn)0x86EB6DD8;
	CreateInterfaceFn vguimat = (CreateInterfaceFn)0x84E458D8;
	CreateInterfaceFn matsys = (CreateInterfaceFn)0x84990220;
	CreateInterfaceFn vstdlib = (CreateInterfaceFn)0x82E10000;
	CreateInterfaceFn filesys = (CreateInterfaceFn)0x8304A5E8;

	/*
		file system interfaces
	*/
	this->FileSystem = (IFileSystem*)filesys("VFileSystem017", NULL);
	DbgPrint("FileSystem...done 0x%p\n", (void*)this->FileSystem);

	/*
		engine interfaces
	*/
	this->EngineClient = (IVEngineClient*)engine(VENGINE_CLIENT_INTERFACE_VERSION_13, NULL);
	DbgPrint("EngineClient...done 0x%p\n", (void*)this->EngineClient);

	this->Trace = (IEngineTrace*)engine("EngineTraceClient003", 0);
	DbgPrint("Trace...done 0x%p\n", (void*)this->Trace);

	this->RenderView = (IVRenderView*)engine("VEngineRenderView013", 0);
	DbgPrint("RenderView...done 0x%p\n", (void*)this->RenderView);

	this->ModelRenderer = (IVModelRenderer*)engine("VEngineModel016", 0);
	DbgPrint("ModelRenderer...done 0x%p\n", (void*)this->ModelRenderer);

	this->ModelInfo = (CModelInfo*)engine("VModelInfoClient004", 0);
	DbgPrint("ModelInfo...done 0x%p\n", (void*)this->ModelInfo);

	this->EngineVGui = (IEngineVGui*)engine(VENGINE_VGUI_INTERFACE_VERSION, 0);
	DbgPrint("EngineVGui...done 0x%p\n", (void*)this->EngineVGui);	

	this->EventManager = (IGameEventManager2*)engine("GAMEEVENTSMANAGER002", 0);
	DbgPrint("EventManager...done 0x%p\n", (void*)this->EventManager);	

	this->EntityList = (IClientEntityList*)client("VClientEntityList003", 0);
	DbgPrint("EntityList...done 0x%p\n", (void*)this->EntityList);

	this->Prediction = (IPrediction*)client("VClientPrediction001", 0);
	DbgPrint("Prediction...done 0x%p\n", (void*)this->Prediction);

	this->GameMovement = (IGameMovement*)client("GameMovement001", 0);
	DbgPrint("GameMovement...done 0x%p\n", (void*)this->GameMovement);

	this->Client = (IVClient*)client("VClient016", 0);
	DbgPrint("Client...done 0x%p\n", (void*)this->Client);
	/*
		material system interfaces
	*/

	this->MaterialSystem = (IMaterialSystem*)matsys("VMaterialSystem080", 0);
	DbgPrint("MaterialSystem...done 0x%p\n", (void*)this->MaterialSystem);

	/*
		vgui2 interfaces
	*/

	this->Panel = (IPanel*)vgui2("VGUI_Panel009", 0);
	DbgPrint("Panel...done 0x%p\n", (void*)this->Panel);

	/*
		vgui material surface interfaces
	*/
	this->Surface = (ISurface*)vguimat("VGUI_Surface031", 0);
	DbgPrint("Surface...done 0x%p\n", (void*)this->Surface);

	/*
		vstdlib interfaces
	*/
	this->Cvar = (IEngineCvar*)vstdlib("VEngineCvar007", 0);
	DbgPrint("Cvar...done 0x%p\n", (void*)this->Cvar);


	/*
		misc global pointers
	*/

	this->MoveHelper = reinterpret_cast<IMoveHelper*>(*(PDWORD)0x8811839C);
	this->GlobalVars = reinterpret_cast<CGlobalVarsBase*>(*(PDWORD)0x89F475D4);
	//DbgPrint("GlobalVars...done");
	//DbgPrint("Frametime: %.6f", GlobalVars->frametime);
	return true;
}

extern "C" player_info_t* GetPlayerInfo(int iEnt)
{
	static player_info_t sPlayer;
	g_pInterfaces->EngineClient->GetPlayerInfo(iEnt, &sPlayer);

	return &sPlayer;
}