#ifndef INTERFACES_H
#define INTERFACES_H
#pragma once

#include "IVEngineClient.h"
#include "IPanel.h"
#include "ISurface.h"
#include "IClientEntityList.h"
#include "IEngineTrace.h"
#include "IVRenderView.h"
#include "IVModelRenderer.h"
#include "IMaterialSystem.h"
#include "IPrediction.h"
#include "IGameMovement.h"
#include "IMoveHelper.h"
#include "GlobalVarsBase.h"
#include "IVClient.h"
#include "IVEngineVGui.h"
#include "IFileSystem.h"
#include "IVModelInfoClient.h"
#include "IEngineCvar.h"
#include "IGameEvent.h"

#include "common.h"

class CInterfaces
{
public:
	CInterfaces(void);
	~CInterfaces(void);

public:
	IVEngineClient* EngineClient;
	IClientEntityList* EntityList;
	CGlobalVarsBase* GlobalVars;
	IEngineTrace* Trace;
	IVRenderView* RenderView;
	IVModelRenderer* ModelRenderer;
	IMaterialSystem* MaterialSystem;
	CModelInfo* ModelInfo;
	IPanel* Panel;
	ISurface* Surface;
	IPrediction* Prediction;
	IGameMovement* GameMovement;
	IMoveHelper* MoveHelper;
	IVClient* Client;
	IEngineVGui* EngineVGui;
	IFileSystem* FileSystem;
	IEngineCvar* Cvar;
	IGameEventManager2* EventManager;
private:
	bool _initialize();
};
extern CInterfaces* g_pInterfaces;
#endif //INTERFACES_H