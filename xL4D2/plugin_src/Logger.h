#ifndef LOGGER_H
#define LOGGER_H

#include "common.h"
#include "IOHelper.h"

class CLogger
{
public:
	CLogger();

public:
	void LogMessage(const char* frmt, ...);

private:
	DWORD GetLogSize();
	DWORD CreateLogFile();

private:
	HANDLE m_hLogFile;
	CHAR m_szFullLogPath;
	DWORD m_dwLogSize;
};

#endif //LOGGER_H