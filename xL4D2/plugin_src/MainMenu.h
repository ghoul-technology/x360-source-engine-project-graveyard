#ifndef MAINMENU_H
#define MAINMENU_H

#pragma once

#include "common.h"
#include "RenderManager.h"

class CMainMenu
{
public:
	CMainMenu();
	~CMainMenu();

	void Draw();

private:
	void Open();
	void Close();

	void DrawBasePanel();
	void DrawControls();

private:
	HFont m_hTitleFont;
	HFont m_hSectionFont;
	HFont m_hTextFont;

	Color m_BackgroundColor;

	bool m_bShown;

};

#endif // MAINMENU_H