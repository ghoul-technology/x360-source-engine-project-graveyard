#ifndef MATHUTILS_H
#define MATHUTILS_H

#pragma once

#include <float.h>
#include <cmath>

//#include "Entity.h"
#include "QAngle.h"
#include "Vector.h"
#include "IClientEntity.h"
#include "common.h"

/*
	Standard math macros
*/
#define PI 3.14159265358979323846f
#define PI_F		((float)(M_PI))
#define DEG2RAD( x ) ( ( float )( x ) * ( float )( ( float )( PI ) / 180.0f ) )
#define RAD2DEG( x ) ( ( float )( x ) * ( float )( 180.0f / ( float )( PI ) ) )
#define RADPI 57.295779513082f
#define rad(a) a * 0.01745329251

namespace MathUtils
{
	void AngleVectors(const Vector &angles, Vector *forward);
	void VectorTransform(const Vector& in1, const matrix3x4& in2, Vector& out);
	void SinCos(float a, float* s, float*c);
	void VectorAngles(Vector forward, Vector &angles);
	void AngleVectors(const Vector &angles, Vector *forward, Vector *right, Vector *up);
	void Normalize(Vector &vIn, Vector &vOut);
	void CalcAngle(Vector src, Vector dst, Vector &angles);
	bool IsVisible(IClientEntity* pLocal, IClientEntity* pEntity, int BoneID);
	void CalcAngleYawOnly(Vector src, Vector dst, Vector &angles);
	void NormalizeVector(Vector& vec);

	inline Vector VectorAngles2( const Vector& vecDirection )
	{
		float fPitch, fYaw = 0.f;

		if (vecDirection.x && vecDirection.y)
		{
			fPitch = RAD2DEG( std::atan2f( -vecDirection.z, vecDirection.Length2D() ) );
			fPitch += fPitch < 0.f ? 360.f : 0.f;

			fYaw = RAD2DEG( std::atan2f( vecDirection.y, vecDirection.x ) );
			fYaw += fYaw < 0.f ? 360.f : 0.f;
		}
		else
		{
			fPitch = vecDirection.z > 0.f ? 270.f : 90.f;
		}

		return Vector( fPitch, fYaw, 0.f );
	}

	inline float NormalizeVector2( Vector& vec )
	{
		const float fLength = vec.Length();
		vec = fLength ? vec / fLength : Vector();

		return fLength;
	}
	inline Vector GetBoneAngles( const Vector& BonePos, const Vector& LocalEyeAngles )
	{
		Vector vecForward = BonePos - LocalEyeAngles;
		NormalizeVector2( vecForward );

		Vector vecAngles = VectorAngles2(vecForward);
		return vecAngles;
	}

	inline unsigned long& FloatBits(vec_t& f)
	{
		return *reinterpret_cast<unsigned long*>(&f);
	}
	inline bool IsFinite(vec_t f)
	{
		return ((FloatBits(f) & 0x7F800000) != 0x7F800000);
	}
	inline float DotProduct(const Vector& a, const Vector& b)
	{
		return (a.x * b.x + a.y * b.y + a.z * b.z);
	}

	inline float WorldDistance( const Vector& vecStart, const Vector& vecEnd )
	{
		float fX = powf( vecEnd.x - vecStart.x, 2.f );
		float fY = powf( vecEnd.y - vecStart.y, 2.f );
		float fZ = powf( vecEnd.z - vecStart.z, 2.f );

		return sqrtf( fX + fY + fZ );
	}
}



#endif //MATHUTILS_H