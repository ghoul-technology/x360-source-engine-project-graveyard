#ifndef NETVARS_H
#define NETVARS_H

#pragma once

#include <vector>
#include <stdint.h>
#include <initializer_list>
		

#include "ClientProps.h"
#include "Interfaces.h"
#include "CRC32.h"
#include "common.h"


#define GET_NETVAR(table, ...) NetvarManager::Instance()->GetOffset(table, __VA_ARGS__)

class NetvarDatabase;
struct NetvarTable;

struct NetVarInfo_t
{
#ifdef DUMP_NETVARS
	char szTableName[128];
	char szPropName[128];
#endif DUMP_NETVARS
	DWORD_PTR dwCRC32;
	DWORD_PTR dwOffset;
};

struct RecvTable;

class CNetVar
{
public:

	CNetVar();

	void RetrieveClasses();
	void LogNetVar(RecvTable *table, int align);
	DWORD_PTR GetNetVar(DWORD_PTR dwCRC32);
	void HookProxies();

private:
	std::vector<NetVarInfo_t>vars;
};

#ifdef DUMP_NETVARS
#define NETVAR_FILENAME "netvars.txt"
#endif

extern CNetVar* g_pNetVar;

#endif // NETVARS_H