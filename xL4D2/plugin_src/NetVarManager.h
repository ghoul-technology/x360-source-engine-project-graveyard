#ifndef NETVARMANAGER_H
#define NETVARMANAGER_H

#pragma once

#include <utility>
#include <unordered_map>
#include <memory>
#include <string>
#include "NetVar.h"
#include "ClientProps.h"
#include "common.h"

class NetvarManager
{
private:
	static NetvarManager* instance;

	NetvarManager();
	~NetvarManager();

	NetvarManager(const NetvarManager&);

public:
	static NetvarManager* Instance()
	{
		if (!instance) instance = new NetvarManager;
		return instance;
	}

	void CreateDatabase();
	void DestroyDatabase();

	void Dump(std::ostream& stream);
	void Dump(const std::string& file);

	int GetNetvarCount() { return m_netvarCount; }
	int GetTableCount() { return m_tableCount; }
	/*
	template<typename ...Args>
	uint32_t GetOffset(const std::string& szTableName, Args&&... args)
	{
		return GetOffset(szTableName, { std::forward<Args>(args)... });
	}*/
private:
	std::unique_ptr<NetvarTable>  InternalLoadTable(RecvTable* pRecvTable, uint32_t offset);
	void                          Dump(std::ostream& output, NetvarTable& table, int level);
	uint32_t                      GetOffset(const std::string& szTableName, const std::initializer_list<std::string>& props);

private:
	std::unique_ptr<NetvarDatabase>    m_pDatabase;
	uint32_t                           m_tableCount;
	uint32_t                           m_netvarCount;
};

#endif // NETVARMANAGER_H