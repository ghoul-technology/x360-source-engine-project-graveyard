#pragma once
#include "RenderManager.h"


#define _CRT_SECURE_NO_WARNINGS

#define M_PI 3.14159265358979323846


RECT RenderManager::GetViewport()
{
	RECT Viewport = { 0, 0, 0, 0 };
	int w, h;
	g_pInterfaces->EngineClient->GetScreenSize(w, h);
	Viewport.right = w; Viewport.bottom = h;
	return Viewport;
}

void RenderManager::Clear(int x, int y, int w, int h, Color color)
{
	g_pInterfaces->Surface->DrawSetColor(color);
	g_pInterfaces->Surface->DrawFilledRect(x, y, x + w, y + h);
}

void RenderManager::Outline(float x, float y, float w, float h, const Color color)
{
	g_pInterfaces->Surface->DrawSetColor(color);
	g_pInterfaces->Surface->DrawOutlinedRect(x, y, x + w, y + h);
}
void RenderManager::OutlinedRect(int x, int y, int w, int h, Color color_out, Color color_in)
{
	g_pInterfaces->Surface->DrawSetColor(color_in);
	g_pInterfaces->Surface->DrawFilledRect(x, y, x + w, y + h);

	g_pInterfaces->Surface->DrawSetColor(color_out);
	g_pInterfaces->Surface->DrawOutlinedRect(x, y, x + w, y + h);
}
void RenderManager::drawRECT(int x1, int y1, int x2, int y2, Color clr)
{
	g_pInterfaces->Surface->DrawSetColor(clr);
	g_pInterfaces->Surface->DrawFilledRect(x1, y1, x2, y2);
}

void RenderManager::DrawRectRainbow(int x, int y, int width, int height, float flSpeed, float &flRainbow,float alpha)
{
	Color colColor(0, 0, 0);

	flRainbow += flSpeed;
	if (flRainbow > 1.f) flRainbow = 0.f;

	for (int i = 0; i < width; i++)
	{
		float hue = (1.f / (float)width) * i;
		hue -= flRainbow;
		if (hue < 0.f) hue += 1.f;

		Color colRainbow = colColor.FromHSB(hue, 1.f, 1.f);
		colRainbow.SetAlpha(alpha);
		RenderManager::DrawRect(x + i, y, 1, height, colRainbow);
	}
}
void RenderManager::Line(int x, int y, int x2, int y2, Color color)
{
	g_pInterfaces->Surface->DrawSetColor(color);
	g_pInterfaces->Surface->DrawLine(x, y, x2, y2);
}

void RenderManager::Line_3(Vector2D start_pos, Vector2D end_pos, Color col)
{
	RenderManager::Line(start_pos.x, start_pos.y, end_pos.x, end_pos.y, col);
}
	/*
void RenderManager::PolyLine(int *x, int *y, int count, Color color)
{

	g_pInterfaces->Surface->DrawSetColor(color);
	g_pInterfaces->Surface->DrawPolyLine(x, y, count);
	
}
*/
/*
void RenderManager::TexturedPolygon(int n, std::vector<Vertex_t> vertice, Color color)
{
	
	static int texture_id = g_pInterfaces->Surface->CreateNewTextureID(true); // 
	static unsigned char buf[4] = { 255, 255, 255, 255 };
	g_pInterfaces->Surface->DrawSetTextureRGBA(texture_id, buf, 1, 1); //
	g_pInterfaces->Surface->DrawSetColor(color); //
	g_pInterfaces->Surface->DrawSetTexture(texture_id); //
	g_pInterfaces->Surface->DrawTexturedPolygon(n, vertice.data()); //
	
}
*/


void RenderManager::DrawOutlinedRect(int x, int y, int w, int h, Color col)
{
	g_pInterfaces->Surface->DrawSetColor(col);
	g_pInterfaces->Surface->DrawOutlinedRect(x, y, x + w, y + h);
}

void RenderManager::DrawLine(int x0, int y0, int x1, int y1, Color col)
{
	g_pInterfaces->Surface->DrawSetColor(col);
	g_pInterfaces->Surface->DrawLine(x0, y0, x1, y1);
}
void RenderManager::DrawRect(int x, int y, int w, int h, Color col)
{
	g_pInterfaces->Surface->DrawSetColor(col);
	g_pInterfaces->Surface->DrawFilledRect(x, y, x + w, y + h);
}

void RenderManager::DrawFilledRect(int x1, int y1, int x2, int y2, Color color)
{
	g_pInterfaces->Surface->DrawSetColor(color);
	g_pInterfaces->Surface->DrawFilledRect(x1, y1, x2, y2);
}

void RenderManager::DrawEmptyRect(int x1, int y1, int x2, int y2, Color color, unsigned char ignore_flags)
{
	g_pInterfaces->Surface->DrawSetColor(color);

	if (!(ignore_flags & 0x1))
		g_pInterfaces->Surface->DrawLine(x1, y1, x2, y1);

	if (!(ignore_flags & 0x10))
		g_pInterfaces->Surface->DrawLine(x2, y1, x2, y2);

	if (!(ignore_flags & 0x100))
		g_pInterfaces->Surface->DrawLine(x2, y2, x1, y2);

	if (!(ignore_flags & 0x1000))
		g_pInterfaces->Surface->DrawLine(x1, y2, x1, y1);
}



void RenderManager::rect(int x, int y, int w, int h, Color color)
{
	g_pInterfaces->Surface->DrawSetColor(color);
	g_pInterfaces->Surface->DrawFilledRect(x, y, x + w, y + h);
}
void RenderManager::outlineyeti(int x, int y, int w, int h, Color color)
{
	g_pInterfaces->Surface->DrawSetColor(color);
	g_pInterfaces->Surface->DrawOutlinedRect(x, y, x + w, y + h);
}

void RenderManager::gradient_verticle(int x, int y, int w, int h, Color c1, Color c2)
{
	RenderManager::rect(x, y, w, h, c1);
	BYTE first = c2.r();
	BYTE second = c2.g();
	BYTE third = c2.b();
	for (int i = 0; i < h; i++)
	{
		float fi = i, fh = h;
		float a = fi / fh;
		DWORD ia = a * 255;
		RenderManager::rect(x, y + i, w, 1, Color(first, second, third, ia));
	}
}

void RenderManager::outlined_rectyeti(int x, int y, int w, int h, Color color_out, Color color_in)
{
	g_pInterfaces->Surface->DrawSetColor(color_in);
	g_pInterfaces->Surface->DrawFilledRect(x, y, x + w, y + h);

	g_pInterfaces->Surface->DrawSetColor(color_out);
	g_pInterfaces->Surface->DrawOutlinedRect(x, y, x + w, y + h);
}
bool RenderManager::TransformScreen(const Vector& in, Vector& out)
{
	const matrix3x4& worldToScreen = g_pInterfaces->EngineClient->WorldToScreenMatrix(); // matrix



	int ScrW, ScrH;

	g_pInterfaces->EngineClient->GetScreenSize(ScrW, ScrH);

	float w = worldToScreen[3][0] * in[0] + worldToScreen[3][1] * in[1] + worldToScreen[3][2] * in[2] + worldToScreen[3][3];
	out.z = 0; // 0 poniewaz z nie jest nam potrzebne | uzywamy tylko wysokosci i szerokosci (x,y)
	if (w > 0.01)
	{
		float inverseWidth = 1 / w; // inverse na 1 pozycje ekranu
		out.x = (ScrW / 2) + (0.5 * ((worldToScreen[0][0] * in[0] + worldToScreen[0][1] * in[1] + worldToScreen[0][2] * in[2] + worldToScreen[0][3]) * inverseWidth) * ScrW + 0.5);
		out.y = (ScrH / 2) - (0.5 * ((worldToScreen[1][0] * in[0] + worldToScreen[1][1] * in[1] + worldToScreen[1][2] * in[2] + worldToScreen[1][3]) * inverseWidth) * ScrH + 0.5);
		return true;
	}
	return false;
}
bool RenderManager::WorldToScreen(const Vector& in, Vector& out)
{
	if (RenderManager::TransformScreen(in, out)) {
		int w, h;
		g_pInterfaces->EngineClient->GetScreenSize(w, h);
		out.x = (w / 2.0f) + (out.x * w) / 2.0f;
		out.y = (h / 2.0f) - (out.y * h) / 2.0f;
		return true;
	}
	return false;
}

void RenderManager::Text(int x, int y, Color color, DWORD font, const char* text)
{
	size_t origsize = strlen(text) + 1;
	const size_t newsize = 100;
	size_t convertedChars = 0;
	wchar_t wcstring[newsize];
	mbstowcs_s(&convertedChars, wcstring, origsize, text, _TRUNCATE);

	g_pInterfaces->Surface->DrawSetTextFont(font);

	g_pInterfaces->Surface->DrawSetTextColor(color);
	g_pInterfaces->Surface->DrawSetTextPos(x, y);
	g_pInterfaces->Surface->DrawPrintText(wcstring, wcslen(wcstring));
	return;
}
void RenderManager::Text(int x, int y, Color color, DWORD font, const wchar_t* text)
{
	g_pInterfaces->Surface->DrawSetTextFont(font);
	g_pInterfaces->Surface->DrawSetTextColor(color);
	g_pInterfaces->Surface->DrawSetTextPos(x, y);
	g_pInterfaces->Surface->DrawPrintText(text, wcslen(text));
}

void RenderManager::TEXTUNICODE(int x, int y, const char* _Input, int font, Color color)
{
	wchar_t buffer[36];
	if (MultiByteToWideChar(CP_UTF8, 0, _Input, -1, buffer, 36) > 0)
	{
		g_pInterfaces->Surface->DrawSetTextColor(color);
		g_pInterfaces->Surface->DrawSetTextFont(font);
		g_pInterfaces->Surface->DrawSetTextPos(x, y);
		g_pInterfaces->Surface->DrawPrintText(buffer, wcslen(buffer));
	}
}



RECT RenderManager::GetTextSize(DWORD font, const char* text)
{
	size_t origsize = strlen(text) + 1;
	const size_t newsize = 100;
	size_t convertedChars = 0;
	wchar_t wcstring[newsize];
	mbstowcs_s(&convertedChars, wcstring, origsize, text, _TRUNCATE);

	RECT rect; int x, y;
	g_pInterfaces->Surface->GetTextSize(font, wcstring, x, y);
	rect.left = x; rect.bottom = y;
	rect.right = x;
	return rect;
}	


void RenderManager::GradientV(int x, int y, int w, int h, Color c1, Color c2)
{
	Clear(x, y, w, h, c1);
	BYTE first = c2.r();
	BYTE second = c2.g();
	BYTE third = c2.b();
	for (int i = 0; i < h; i++)
	{
		float fi = i, fh = h;
		float a = fi / fh;
		DWORD ia = a * 255;
		Clear(x, y + i, w, 1, Color(first, second, third, ia));
	}
}

void RenderManager::DrawCircle(float x, float y, float r, float segments, Color color)
{
	g_pInterfaces->Surface->DrawSetColor(color);
	g_pInterfaces->Surface->DrawOutlinedCircle(x, y, r, segments);
}

int TweakColor(int c1, int c2, int variation)
{
	if (c1 == c2)
		return c1;
	else if (c1 < c2)
		c1 += variation;
	else
		c1 -= variation;
	return c1;
}

Color RenderManager::color_spectrum_pen(int x, int y, int w, int h, Vector stx)
{
	int div = w / 6;
	int phase = stx.x / div;
	float t = ((int)stx.x % div) / (float)div;
	int r, g, b;

	switch (phase)
	{
	case(0):
		r = 255;
		g = 255 * t;
		b = 0;
		break;
	case(1):
		r = 255 * (1.f - t);
		g = 255;
		b = 0;
		break;
	case(2):
		r = 0;
		g = 255;
		b = 255 * t;
		break;
	case(3):
		r = 0;
		g = 255 * (1.f - t);
		b = 255;
		break;
	case(4):
		r = 255 * t;
		g = 0;
		b = 255;
		break;
	case(5):
		r = 255;
		g = 0;
		b = 255 * (1.f - t);
		break;
	}

	float sat = stx.y / h;
	return Color(r + sat * (128 - r), g + sat * (128 - g), b + sat * (128 - b), 255);
}

void RenderManager::gradient_horizontal(int x, int y, int w, int h, Color c1, Color c2)
{
	RenderManager::rect(x, y, w, h, c1);
	BYTE first = c2.r();
	BYTE second = c2.g();
	BYTE third = c2.b();
	for (int i = 0; i < w; i++)
	{
		float fi = i, fw = w;
		float a = fi / fw;
		DWORD ia = a * 255;
		RenderManager::rect(x + i, y, 1, h, Color(first, second, third, ia));
	}
}

void RenderManager::GradientB(int x, int y, int w, int h, Color color1, Color color2, int variation)
{
	int r1 = color1.r();
	int g1 = color1.g();
	int b1 = color1.b();
	int a1 = color1.a();

	int r2 = color2.r();
	int g2 = color2.g();
	int b2 = color2.b();
	int a2 = color2.a();

	for (int i = 0; i <= w; i++)
	{
		RenderManager::DrawRect(x + i, y, 1, h, Color(r1, g1, b1, a1));
		r1 = TweakColor(r1, r2, variation);
		g1 = TweakColor(g1, g2, variation);
		b1 = TweakColor(b1, b2, variation);
		a1 = TweakColor(a1, a2, variation);
	}
}

void RenderManager::Color_spectrum(int x, int y, int w, int h)
{
	/*
	static int GradientTexture = 0;
	static std::unique_ptr<Color[]> Gradient = nullptr;
	if (!Gradient)
	{
		Gradient = std::make_unique<Color[]>(w * h);

		for (int i = 0; i < w; i++)
		{
			int div = w / 6;
			int phase = i / div;
			float t = (i % div) / (float)div;
			int r, g, b;

			switch (phase)
			{
			case(0):
				r = 255;
				g = 255 * t;
				b = 0;
				break;
			case(1):
				r = 255 * (1.f - t);
				g = 255;
				b = 0;
				break;
			case(2):
				r = 0;
				g = 255;
				b = 255 * t;
				break;
			case(3):
				r = 0;
				g = 255 * (1.f - t);
				b = 255;
				break;
			case(4):
				r = 255 * t;
				g = 0;
				b = 255;
				break;
			case(5):
				r = 255;
				g = 0;
				b = 255 * (1.f - t);
				break;
			}

			for (int k = 0; k < h; k++)
			{
				float sat = k / (float)h;
				int _r = r + sat * (128 - r);
				int _g = g + sat * (128 - g);
				int _b = b + sat * (128 - b);

				*reinterpret_cast<Color*>(Gradient.get() + i + k * w) = Color(_r, _g, _b);
			}
		}

		GradientTexture = g_pInterfaces->Surface->CreateNewTextureID(true);
		g_pInterfaces->Surface->DrawSetTextureRGBA(GradientTexture, (unsigned char*)Gradient.get(), w, h);
	}
	g_pInterfaces->Surface->DrawSetColor(Color(255, 255, 255, 255));
	g_pInterfaces->Surface->DrawSetTexture(GradientTexture);
	g_pInterfaces->Surface->DrawTexturedRect(x, y, x + w, y + h);
	*/
}
Color RenderManager::Color_spectrum_pen(int x, int y, int w, int h, Vector stx)
{
	int div = w / 6;
	int phase = stx.x / div;
	float t = ((int)stx.x % div) / (float)div;
	float r, g, b;

	switch (phase)
	{
	case(0):
		r = 255;
		g = 255 * t;
		b = 0;
		break;
	case(1):
		r = 255 * (1.f - t);
		g = 255;
		b = 0;
		break;
	case(2):
		r = 0;
		g = 255;
		b = 255 * t;
		break;
	case(3):
		r = 0;
		g = 255 * (1.f - t);
		b = 255;
		break;
	case(4):
		r = 255 * t;
		g = 0;
		b = 255;
		break;
	case(5):
		r = 255;
		g = 0;
		b = 255 * (1.f - t);
		break;
	}

	float sat = stx.y / h;
	return Color(r + sat * (128 - r), g + sat * (128 - g), b + sat * (128 - b), 255);
}


void RenderManager::DrawTexturedPoly(int n, Vertex_t* vertice, Color col)
{
	//static int texture_id = g_pInterfaces->Surface->CreateNewTextureID(true);
	//static unsigned char buf[4] = { 255, 255, 255, 255 };
	//g_pInterfaces->Surface->DrawSetTextureRGBA(texture_id, buf, 1, 1);
	//g_pInterfaces->Surface->DrawSetColor(col);
	//g_pInterfaces->Surface->DrawSetTexture(texture_id);
	//g_pInterfaces->Surface->DrawTexturedPolygon(n, vertice);
}

void RenderManager::DrawFilledCircle(Vector2D center, Color color, float radius, float points)
{
	std::vector<Vertex_t> vertices;
	float step = (float)M_PI * 2.0f / points;

	for (float a = 0; a < (M_PI * 2.0f); a += step)
		vertices.push_back(Vertex_t(Vector2D(radius * cosf(a) + center.x, radius * sinf(a) + center.y)));

	DrawTexturedPoly((int)points, vertices.data(), color);
}

void RenderManager::GradientSideways(int x, int y, int w, int h, Color color1, Color color2, int variation)
{
	int r1 = color1.r();
	int g1 = color1.g();
	int b1 = color1.b();
	int a1 = color1.a();

	int r2 = color2.r();
	int g2 = color2.g();
	int b2 = color2.b();
	int a2 = color2.a();

	for (int i = 0; i <= w; i++)
	{
		DrawRect(x + i, y, 1, h, Color(r1, g1, b1, a1));
		r1 = TweakColor(r1, r2, variation);
		g1 = TweakColor(g1, g2, variation);
		b1 = TweakColor(b1, b2, variation);
		a1 = TweakColor(a1, a2, variation);
	}
}
