#ifndef USERCMD_H
#define USERCMD_H

#pragma once

class CUserCmd
{
public:
	virtual ~CUserCmd() {};
	int command_number;
	int tick_count;
	Vector viewangles;
	float forwardmove;
	float sidemove;
	float upmove;
	int buttons;
	BYTE impulse;
	int weaponselect;
	int weaponsubtype;
	int random_seed;
	short mousedx;
	short mousedy;
	bool hasbeenpredicted;
};

#endif //USERCMD_H