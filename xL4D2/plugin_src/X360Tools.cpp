#include "X360Tools.hpp"

pDmSetMemory DevSetMemory = NULL;
pDmGetMemory DevGetMemory = NULL;

void MessageBox(LPCWSTR Text, LPCWSTR Caption) {
	LPCWSTR* text;
	text = new LPCWSTR[1];
	text[0] = L"Ok";
	PXOVERLAPPED over = new XOVERLAPPED();
	memset(over, 0, sizeof(over));
	PMESSAGEBOX_RESULT result = new MESSAGEBOX_RESULT();
	memset(result, 0, sizeof(result));

	while(XShowMessageBoxUI(0, Caption, Text, 1, text, 0, XMB_NOICON, result, over) == ERROR_ACCESS_DENIED)
		Sleep(500);
	while(!XHasOverlappedIoCompleted(over))
		Sleep(500);
}

UINT32 ResolveFunct(char* modname, HANDLE ord) {
	UINT32 ret = 0, ptr2 = 0;
	HANDLE ptr32 = 0;

	ret = XexGetModuleHandle(modname, &ptr32);
	if(ret == 0) {
		ret = XexGetProcedureAddress(ptr32, (DWORD)ord, &ptr2);
		if(ptr2 != 0)
			return(ptr2);
	}
	return(0);
}

DWORD GetPressedButtons() {
	XINPUT_STATE xstate;

	if (XInputGetState(0, &xstate) == ERROR_SUCCESS)
		return xstate.Gamepad.wButtons;
	else
		return (DWORD)-1;
}

VOID DbgPrint( const CHAR* strFormat, ... ) 
{

	CHAR buffer[ 1000 ];

	va_list pArgList;
	va_start( pArgList, strFormat );
	vsprintf_s( buffer, 1000, strFormat, pArgList );

	va_end(pArgList);

    printf("[xL4D2] %s\n", buffer);
}

FARPROC ResolveFunction(CHAR* pszModuleName, DWORD dwOrdinal) 
{
	HMODULE hHandle = GetModuleHandle(pszModuleName);
	return (hHandle == NULL) ? NULL : GetProcAddress(hHandle, (LPCSTR)dwOrdinal);
}

HRESULT PatchInJump(DWORD* pdwAddr, DWORD dwDest, BOOL bLinked) 
{
	if(DevSetMemory == NULL)
		DevSetMemory = (HRESULT (__cdecl *)(LPVOID, DWORD, LPCVOID, LPDWORD)) (ResolveFunction("xbdm.xex", 40));
	if(IS_DEVKIT)
	{
		DWORD dwData[4];
		{
			dwData[0] = 0x3D600000 + ((dwDest >> 16) & 0xFFFF);
			if(dwDest & 0x8000) dwData[0] += 1;
			dwData[1] = 0x396B0000 + (dwDest & 0xFFFF);
			dwData[2] = 0x7D6903A6;
			dwData[3] = bLinked ? 0x4E800421 : 0x4E800420;
		}
		DWORD dwCBRet = 0;
		HRESULT hr = DevSetMemory((LPVOID)pdwAddr, 16, dwData, &dwCBRet);
		if (hr != XBDM_NOERR || dwCBRet != 16)
		{
			DbgPrint("DmSetMemory failed. HRESULT = %08X cbRet = %d\n", hr, dwCBRet);
			return E_FAIL;
		}
		else
			return ERROR_SUCCESS;
	}
	else
	{
		pdwAddr[0] = 0x3D600000 + ((dwDest >> 16) & 0xFFFF);
		if(dwDest & 0x8000) pdwAddr[0] += 1;
		pdwAddr[1] = 0x396B0000 + (dwDest & 0xFFFF);
		pdwAddr[2] = 0x7D6903A6;
		pdwAddr[3] = bLinked ? 0x4E800421 : 0x4E800420;
		return ERROR_SUCCESS;
	}
	return E_FAIL;
}

HRESULT SetMemory(VOID* pvDest, VOID* pvSrc, DWORD dwLen) 
{

	// Try to resolve our function
	if(DevSetMemory == NULL)
		DevSetMemory = (pDmSetMemory)ResolveFunction("xbdm.xex", 40);

	//Set Memory
	if(DevSetMemory == NULL) 
	{
		memcpy(pvDest, pvSrc, dwLen);
		return ERROR_SUCCESS;
	} 
	else if(DevSetMemory(pvDest, dwLen, pvSrc, NULL) == MAKE_HRESULT(0, 0x2DA, 0))
			return ERROR_SUCCESS;
	return E_FAIL;
}

HRESULT GetMemory(DWORD dwAddr, BYTE arru8Val[], size_t u32lLength)
{
	if(DevGetMemory == NULL)
		DevGetMemory = (pDmGetMemory)ResolveFunction("xbdm.xex", 10);
	//Get Memory
	if(DevGetMemory == NULL) 
	{
		memcpy((void*)arru8Val, (void*)dwAddr, u32lLength);
		return ERROR_SUCCESS;
	} 
	else if(DevGetMemory((void*)dwAddr, u32lLength, (void*)arru8Val, NULL) == MAKE_HRESULT(0, 0x2DA, 0))
			return ERROR_SUCCESS;
	return E_FAIL;
}