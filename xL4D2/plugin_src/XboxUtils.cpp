#include "XboxUtils.h"

pDmSetMemory DevSetMemory = NULL;
pDmGetMemory DevGetMemory = NULL;

FARPROC CXboxUtils::ResolveFunction(CHAR* pszModuleName, DWORD dwOrdinal) 
{
	HMODULE hHandle = GetModuleHandle(pszModuleName);
	return (hHandle == NULL) ? NULL : GetProcAddress(hHandle, (LPCSTR)dwOrdinal);
}

HRESULT CXboxUtils::PatchInJump(DWORD* pdwAddr, DWORD dwDest, BOOL bLinked)
{
	if(DevSetMemory == NULL)
		DevSetMemory = (HRESULT (__cdecl *)(LPVOID, DWORD, LPCVOID, LPDWORD)) (ResolveFunction("xbdm.xex", 40));
	if(IS_DEVKIT)
	{
		DWORD dwData[4];
		{
			dwData[0] = 0x3D600000 + ((dwDest >> 16) & 0xFFFF);
			if(dwDest & 0x8000) dwData[0] += 1;
			dwData[1] = 0x396B0000 + (dwDest & 0xFFFF);
			dwData[2] = 0x7D6903A6;
			dwData[3] = bLinked ? 0x4E800421 : 0x4E800420;
		}
		DWORD dwCBRet = 0;
		HRESULT hr = DevSetMemory((LPVOID)pdwAddr, 16, dwData, &dwCBRet);
		if (hr != XBDM_NOERR || dwCBRet != 16)
		{
			DbgPrint("DmSetMemory failed. HRESULT = %08X cbRet = %d\n", hr, dwCBRet);
			return E_FAIL;
		}
		else
			return ERROR_SUCCESS;
	}
	else
	{
		pdwAddr[0] = 0x3D600000 + ((dwDest >> 16) & 0xFFFF);
		if(dwDest & 0x8000) pdwAddr[0] += 1;
		pdwAddr[1] = 0x396B0000 + (dwDest & 0xFFFF);
		pdwAddr[2] = 0x7D6903A6;
		pdwAddr[3] = bLinked ? 0x4E800421 : 0x4E800420;
		return ERROR_SUCCESS;
	}
	return E_FAIL;
}