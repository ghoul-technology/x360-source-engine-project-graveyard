#ifndef XBOXUTILS_H
#define XBOXUTILS_H

#pragma once

#include "common.h"

#define IS_DEVKIT *(DWORD*)0x8E038610 & 0x8000 ? FALSE : TRUE

typedef HRESULT (*pDmSetMemory)(LPVOID lpbAddr, DWORD cb, LPCVOID lpbBuf, LPDWORD pcbRet);
typedef HRESULT (*pDmGetMemory)(LPCVOID, DWORD, LPVOID, LPDWORD);



class CXboxUtils
{
public:
	static HRESULT PatchInJump(DWORD* pdwAddr, DWORD dwDest, BOOL bLinked);
	static FARPROC ResolveFunction(CHAR* pszModuleName, DWORD dwOrdinal);
	static UINT32 ResolveFunct(char* modname, HANDLE ord);
	static DWORD GetPressedButtons();
	static VOID MessageBox(LPCWSTR Text, LPCWSTR Caption);
};

#endif // XBOXUTILS_H