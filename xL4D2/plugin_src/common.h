// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//
#ifndef COMMON_H
#define COMMON_H
#pragma once

#include <xtl.h>
#include <xboxmath.h>
#include <xbox.h>
// TODO: reference additional headers your program requires here
#include <stdio.h>
#include <stdlib.h>
#include <winbase.h>
#include <ppcintrinsics.h>
#include <fstream>
#include <sstream>
#include <xbdm.h>
#include <map>
#include <xui.h>
#include <memory> // brings in TEMPLATE macros.
#include <string>
#include "kernel.h"

template<typename Fn>
Fn GetFunction( void* Base, int Index )
{
	PDWORD** uVTablePtr = static_cast<PDWORD**>(Base);
	PDWORD* uVTableFnBase = *uVTablePtr;
	PDWORD uAddress = uVTableFnBase[Index];
	return reinterpret_cast<Fn>(uAddress);
};

typedef float vec_t;

typedef float matrix3x4[3][4];

typedef void* (*CreateInterfaceFn)(const char *pName, int *pReturnCode);

#define IS_DEVKIT *(DWORD*)0x8E038610 & 0x8000 ? FALSE : TRUE

template<typename T> inline T* MakePointerGEN( PVOID thisptr, int offset ) 
{ 
	return reinterpret_cast<T*>( reinterpret_cast<UINT_PTR>( thisptr ) + offset ); 
}

typedef struct player_info_s
{
	// network xuid
	DWORD			xuidlow;
	DWORD			xuidhigh;
	// scoreboard information
	char			name[256];
	// local server user ID, unique while server is running
	int				userID;
	// global unique player identifer
	char			guid[33];
	// friends identification number
	DWORD			friendsID;
	// friends name
	char			friendsName[128];
	// true, if player is a bot controlled by game.dll
	bool			fakeplayer;
	// true if player is the HLTV proxy
	bool			ishltv;
	// custom files CRC for this player
	DWORD			customFiles[4];
	// this counter increases each time the server downloaded a new file
	unsigned char	filesDownloaded;
} player_info_t;

#endif //COMMON_H