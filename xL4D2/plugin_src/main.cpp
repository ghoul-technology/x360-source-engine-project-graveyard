#include "common.h"
#include "HackEngine.h"

BOOL APIENTRY DllMain( HANDLE hModule, DWORD dwReason, LPVOID lpReserved ) 
{
	if ( dwReason == DLL_PROCESS_ATTACH ) 
	{
		HANDLE hThread;
		DWORD dwThreadId;
		
		ExCreateThread( 
			&hThread, 
			0, 
			&dwThreadId, 
			( PVOID )XapiThreadStartup , 
			( LPTHREAD_START_ROUTINE )StartEngine, 
			0, 
			0x2 );

		ResumeThread( hThread );
		CloseHandle( hThread );
	}

	return TRUE;
}
