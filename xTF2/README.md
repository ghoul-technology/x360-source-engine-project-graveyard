# Source Engine Project Graveyard

Proof of concept for hooking VMT's and creating interface handles from Source Engine appsystems. 

I believe the only thing that needs to be changed is the SDK paths that are hardcoded to C:\SourceEngine_SDK_0.033

X360 SDK is also hardcoded to look in Program Files x86, if you installed else where, update the include path.

Offsets are based on TU0 bins.

Credits: CRACKbomber
