#pragma once

#include <xtl.h>
#include <xboxmath.h>
#include <stdio.h>
#include <fstream>
#include <sstream>
#include <xbdm.h>
#include <map>
#include "XConfig.h"
#include "xkelib.h"
//Source Engine SDK includes
#include "interface.h"
#include "Color.h"
#include "icvar.h"
#include "convar.h"
#include "cdll_int.h"
#include "vgui/ISurface.h"
#include "vgui/IPanel.h"
#include "tier1/bitbuf.h"
#include "usercmd.h"
#include "VMTHookManager.h"
#include "iinput.h"
#include "icliententitylist.h"
#include "icliententity.h"
#include "iclientunknown.h"
#include "IEngineTrace.h"
using namespace std;
typedef const QAngle&	( * fnGetPunchAngles)( DWORD* );
typedef void*			( * fnWriteUserCmd )( bf_write*, CUserCmd *, CUserCmd* );
typedef void*			( * fnCreateMove )( int, float, bool );
typedef void*			( * fnPaintTraverse )( int, bool, bool );
typedef DWORD*			( * fnSharedRandomInt )( const char*, int, int, int );
typedef DWORD*			( * fnGetLocalTF2Player )();
typedef DWORD*			( * fnGetLocalPlayer )();
typedef struct CScreenSize_t
{
	int iScreenHeight;
	int iScreenWidth;

} CScreenSize;

extern CreateInterfaceFn	g_fnAppSystemFactory;
extern fnWriteUserCmd		g_fnWriteUserCMD;
extern fnCreateMove			g_fnOriginalCreateMove;
extern fnPaintTraverse		g_fnOriginalPaintTraverse;
extern fnSharedRandomInt	g_fnSharedRandomInt;
extern fnGetLocalTF2Player	g_fnGetLocalTF2Player;
extern fnGetLocalPlayer		g_fnGetLocalPlayer;

extern ICvar*				g_pICvar;
extern IEngineTrace*		g_pIEngineTrace;
extern IVEngineClient*		g_pIVEngineClient;
extern vgui::ISurface*		g_pISurface;
extern vgui::IPanel*		g_pIPanel;
extern IBaseClientDLL*		g_pIBaseClientDLL;
extern IInput*				g_pIInput;
extern IClientEntityList*	g_pIEntityList;
extern CVMTHookManager*	g_pIBaseClientDLLHook;
extern CVMTHookManager*	g_pIPanelHook;

extern BOOL g_bIsDevkit;
extern vgui::HFont g_HArial;
extern CScreenSize_t g_ScreenSize;
