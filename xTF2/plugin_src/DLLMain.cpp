#include "Common.h"
#include "TF2Memory.h"
#include "TF2Debug.h"
#include "TF2Hooks.h"




CreateInterfaceFn	g_fnClientDLL	= NULL;
CreateInterfaceFn	g_fnEngineDLL	= NULL;
CreateInterfaceFn	g_fnVGUI2DLL	= NULL;
CreateInterfaceFn	g_fnVGUIMatSurf = NULL;
CreateInterfaceFn	g_fnVSTDLib = NULL;
fnCreateMove		g_fnOriginalCreateMove = NULL;
fnPaintTraverse		g_fnOriginalPaintTraverse = NULL;

ICvar*				g_pICvar = NULL;
IEngineTrace*		g_pIEngineTrace = NULL;
IVEngineClient*		g_pIVEngineClient = NULL;
vgui::ISurface*		g_pISurface = NULL;
vgui::IPanel*		g_pIPanel	= NULL;
IBaseClientDLL*		g_pIBaseClientDLL = NULL;
IInput*				g_pIInput = NULL;
IClientEntityList*	g_pIEntityList = NULL;
CVMTHookManager*	g_pIBaseClientDLLHook = NULL;
CVMTHookManager*	g_pIPanelHook = NULL;
CVMTHookManager*	g_pIMatSurface = NULL;
CGlobalVarsBase *gpGlobals;
BOOL g_bIsDevkit		= FALSE;
vgui::HFont g_HArial;
CScreenSize_t g_ScreenSize;

void InitFactories(int game)
{
	/*tf2
	g_fnClientDLL = (CreateInterfaceFn)0x8756E3D0;
	g_fnEngineDLL = (CreateInterfaceFn)0x8630C3F0;
	g_fnVGUI2DLL = (CreateInterfaceFn)0x837A9448;
	g_fnVGUIMatSurf = (CreateInterfaceFn)0x845A4E00;
	g_fnVSTDLib = (CreateInterfaceFn)0x82997890;
	*/
	switch(game)
	{
	case 1:
		g_fnClientDLL = (CreateInterfaceFn)0x8756E3D0;
		g_fnEngineDLL = (CreateInterfaceFn)0x8630C3F0;
		g_fnVGUI2DLL = (CreateInterfaceFn)0x837A9448;
		g_fnVGUIMatSurf = (CreateInterfaceFn)0x845A4E00;
		g_fnVSTDLib = (CreateInterfaceFn)0x82997890;
	}

}

QAngle GetPunchAngles(DWORD* p_dwPlayer)
{
	fnGetPunchAngles getAngles = (fnGetPunchAngles)0x872C54B8;
	return getAngles(p_dwPlayer);
}

void GetFuncs()
{
}

void HookClientInterfaces()
{
	// IEntityList
	g_pIEntityList = (IClientEntityList*)g_fnClientDLL(VCLIENTENTITYLIST_INTERFACE_VERSION, 0);
	DbgPrint("IClientEntityList...done");

	// IBaseClientDLL
	g_pIBaseClientDLL = (IBaseClientDLL*)g_fnClientDLL(CLIENT_DLL_INTERFACE_VERSION, 0);
	{
		DbgPrint("IBaseClientDLL...done");
		g_pIBaseClientDLLHook = new CVMTHookManager();
		if(g_pIBaseClientDLLHook->bInitialize((PDWORD*)g_pIBaseClientDLL))
		{
			g_fnOriginalCreateMove = (fnCreateMove)g_pIBaseClientDLLHook->dwHookMethod( ( DWORD )CreateMoveHook, 18 );
			//g_pIBaseClientDLLHook->dwHookMethod((DWORD)WriteUserCmdDeltaToBufferHook, 20);
			//DbgPrint("WriteUserCmdDeltaToBufferHook ...done");
		}
	}


}

void HookVGuiInterfaces(int game)
{
	int iFuncPaint = -1;
	DWORD dwCreateFont = 0;
	DWORD dwSetGlyph = 0;

	DWORD dwDrawSetTextPos = 0;
	DWORD dwDrawSetTextFont = 0;
	DWORD dwDrawSetTextColor = 0;
	DWORD dwDrawPrintText = 0;

	switch(iFuncPaint)
	{
	case 1: // tf2
		40;
		break;
	case 2: // l4d2
		41;
		break;
	}
	// ISurface
	g_pISurface = (vgui::ISurface*)g_fnVGUIMatSurf(VGUI_SURFACE_INTERFACE_VERSION, 0);
	g_pIMatSurface = new CVMTHookManager();

	if(g_pIMatSurface->bInitialize((PDWORD*)g_pISurface))
	{
		dwCreateFont = g_pIMatSurface->dwGetMethodAddress(67);
		dwSetGlyph = g_pIMatSurface->dwGetMethodAddress(68);
	}

	DbgPrint("Create font = %i", dwCreateFont);
	DbgPrint("Set Glyph = %i", dwSetGlyph);

	DbgPrint("ISurface...done");

	DWORD font = g_pISurface->CreateFont();

	DbgPrint("Font = %i", font);

	// IPanel
	g_pIPanel = (vgui::IPanel*)g_fnVGUI2DLL(VGUI_PANEL_INTERFACE_VERSION, 0);
	{
		DbgPrint("IPanel...done");
		g_pIPanelHook = new CVMTHookManager();
		if(g_pIPanelHook->bInitialize(( PDWORD* )g_pIPanel))
			g_fnOriginalPaintTraverse = (fnPaintTraverse)g_pIPanelHook->dwHookMethod((DWORD)PaintTraverseHook, 40);
		DbgPrint("PaintTraverse hooked...done");
	}
}

void HookEngineInterfaces()
{
	// ICvar
	g_pICvar = (ICvar*)g_fnVSTDLib(CVAR_INTERFACE_VERSION, 0);
	DbgPrint("ICvar...done");

	//IEngineClient
	g_pIVEngineClient = (IVEngineClient*)g_fnEngineDLL(VENGINE_CLIENT_INTERFACE_VERSION, 0);
	DbgPrint("IVEngineClient...done");

	// IInput
	g_pIInput = (IInput*)0x876194C8;

	// IEngineTrace
	g_pIEngineTrace = (IEngineTrace*)g_fnEngineDLL(INTERFACEVERSION_ENGINETRACE_CLIENT, 0);
	DbgPrint("IEngineTrace...done");
}

void InitializeVGui()
{
	// Initialize fonts
	g_HArial = g_pISurface->CreateFont();
	g_pISurface->SetFontGlyphSet(g_HArial, "Arial", 12, 400, 0, 0, 0);

	// Initialize screen bounds
	g_pIVEngineClient->GetScreenSize(g_ScreenSize.iScreenWidth, g_ScreenSize.iScreenHeight);
}

HRESULT Initialize( )
{
	DbgPrint("Loading...");

	if(g_fnClientDLL != NULL)
		return E_FAIL;

	g_bIsDevkit =  *(DWORD*)0x8E038610 & 0x8000 ? FALSE : TRUE;
	
	InitFactories(1);


	HookClientInterfaces();

	HookVGuiInterfaces(1);

	HookEngineInterfaces();

	InitializeVGui();

	

	DbgPrint("All good");
	return ERROR_SUCCESS;
}

BOOL APIENTRY DllMain(HANDLE hModule, DWORD ul_reason_for_call, LPVOID lpReserved)
{
	switch(ul_reason_for_call)
	{
	case DLL_PROCESS_ATTACH:
		HANDLE hThread; 
		DWORD threadId;
		ExCreateThread(&hThread, 0, &threadId, (VOID*)XapiThreadStartup, (LPTHREAD_START_ROUTINE)Initialize, NULL, 0x2);
		XSetThreadProcessor(hThread, 4);
		SetThreadPriority(hThread, THREAD_PRIORITY_TIME_CRITICAL);
		ResumeThread(hThread);
		return true;
	case DLL_THREAD_ATTACH:
	case DLL_THREAD_DETACH:
	case DLL_PROCESS_DETACH:
		break;
	}
	return TRUE;
}