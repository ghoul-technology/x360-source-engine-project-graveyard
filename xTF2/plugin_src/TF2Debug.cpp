#include "TF2Debug.h"
VOID DbgPrint( const CHAR* strFormat, ... ) 
{

	CHAR buffer[ 1000 ];

	va_list pArgList;
	va_start( pArgList, strFormat );
	vsprintf_s( buffer, 1000, strFormat, pArgList );

	va_end(pArgList);

    printf("[xTF2] %s\n", buffer);
}

