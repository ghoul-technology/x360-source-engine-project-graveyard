#include "TF2Drawing.h"

VOID PrintString( int x, int y, int r, int g, int b, int a, const char* pszText, ... )
{
	if( pszText == NULL )
		return;

	va_list va_alist;
	char szBuffer[1024] = { '\0' };
	wchar_t szString[1024] = { '\0' };

	va_start( va_alist, pszText );
	vsprintf_s( szBuffer, pszText, va_alist );
	va_end( va_alist );

	wsprintfW( szString, L"%S", szBuffer );

	g_pISurface->DrawSetTextPos( x, y );
	g_pISurface->DrawSetTextFont( g_HArial );
	g_pISurface->DrawSetTextColor( r, g, b, a );
	g_pISurface->DrawPrintText( szString, wcslen( szString ) );
}