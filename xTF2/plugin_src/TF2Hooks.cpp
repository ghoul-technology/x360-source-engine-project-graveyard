#include "TF2Hooks.h"
int i = 0;
int imax = 10;
VOID PaintTraverseHook(vgui::VPANEL vguiPanel, bool forceRepaint, bool allowForce)
{
	PrintString( g_ScreenSize.iScreenWidth / 2, 200, 255, 255, 255, 255, "xTF2 by CRACKbomber" );
	g_fnOriginalPaintTraverse(vguiPanel,forceRepaint, allowForce);
}
VOID DefaultSpewFuncHook(DWORD spewType, const char *pGroupName, int nLevel, const Color *pColor, const char* pMsgFormat, va_list args)
{
	char pTempBuffer[5020];
	int len = 0;
	int val= _vsnprintf( &pTempBuffer[len], sizeof( pTempBuffer ) - len - 1, pMsgFormat, args );
	DbgPrint(pTempBuffer);
}
/*(BOOL WriteUserCmdDeltaToBufferHook(bf_write* buf, int from, int to, bool isNewCommand)
{
	CUserCmd nullcmd, *f, *t;

	if(from == -1)
		f = &nullcmd;
	else
	{
		f = g_pIInput->GetUserCmd(from);
		if(!f)
			f = &nullcmd;
	}
	t = g_pIInput->GetUserCmd(to);
	if(!t)
		t = &nullcmd;

	g_fnWriteUserCMD(buf, t, f);

	if(buf->IsOverflowed())
		return false;

	return true;
}*/
VOID CreateMoveHook( int sequence_number, float input_sample_frametime, bool active )
{
	g_fnOriginalCreateMove(sequence_number, input_sample_frametime, active);
}