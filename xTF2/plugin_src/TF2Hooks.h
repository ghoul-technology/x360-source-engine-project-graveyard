#pragma once
#include "Common.h"
#include "TF2Drawing.h"
#include "TF2Debug.h"
VOID PaintTraverseHook(vgui::VPANEL vguiPanel, bool forceRepaint, bool allowForce = true);
VOID DefaultSpewFuncHook(DWORD spewType, const char *pGroupName, int nLevel, const Color *pColor, const char* pMsgFormat, va_list args);
BOOL WriteUserCmdDeltaToBufferHook(bf_write* buf, int from, int to, bool isNewCommand);
VOID CreateMoveHook( int sequence_number, float input_sample_frametime, bool active );