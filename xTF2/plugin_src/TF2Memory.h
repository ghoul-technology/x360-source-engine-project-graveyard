#pragma once
#include "Common.h"
#include "TF2Debug.h"

typedef HRESULT (*pDmSetMemory)(LPVOID lpbAddr, DWORD cb, LPCVOID lpbBuf, LPDWORD pcbRet);
typedef HRESULT (*pDmGetMemory)(LPCVOID, DWORD, LPVOID, LPDWORD);
HRESULT PatchInJump(DWORD* pdwAddr, DWORD dwDest, BOOL bLinked);
HRESULT SetMemory(VOID* pvDest, VOID* pvSrc, DWORD dwLen);
HRESULT GetMemory(DWORD dwAddr, BYTE arru8Val[], size_t u32lLength);