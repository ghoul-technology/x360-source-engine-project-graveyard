#include "TF2Misc.h"

HRESULT CreateSymbolicLink(CHAR* pszDrive, CHAR* pszDeviceName, BOOL bSystem) 
{

	// Setup our path
	CHAR szDestinationDrive[MAX_PATH];
	sprintf_s(szDestinationDrive, MAX_PATH, bSystem ? "\\System??\\%s" : "\\??\\%s", pszDrive);

	// Setup our strings
	ANSI_STRING linkname, devicename;
	RtlInitAnsiString(&linkname, szDestinationDrive);
	RtlInitAnsiString(&devicename, pszDeviceName);

	// Create finally
	NTSTATUS status = ObCreateSymbolicLink(&linkname, &devicename);
	return (status >= 0) ? S_OK : S_FALSE;
}
HRESULT DeleteSymbolicLink(CHAR* pszDrive, BOOL bSystem) 
{

	// Setup our path
	CHAR szDestinationDrive[MAX_PATH];
	sprintf_s(szDestinationDrive, MAX_PATH, bSystem ? "\\System??\\%s" : "\\??\\%s", pszDrive);

	// Setup our string
	ANSI_STRING linkname;
	RtlInitAnsiString(&linkname, szDestinationDrive);
	
	// Delete finally
	NTSTATUS status = ObDeleteSymbolicLink(&linkname);
	return (status >= 0) ? S_OK : S_FALSE;
}
