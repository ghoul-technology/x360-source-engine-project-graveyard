#pragma once
#include "Common.h"

HRESULT CreateSymbolicLink(CHAR* pszDrive, CHAR* pszDeviceName, BOOL bSystem);
HRESULT DeleteSymbolicLink(CHAR* pszDrive, BOOL bSystem);